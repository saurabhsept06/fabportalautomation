
'########################################################################################################################
'###  Function Name                 :  saveJNLP
'###  Purpose                       :  This function will be downloading jnpl file from different env. i.e. FAB1, Fab8 and MTQA staging and save it in the folder.
'###  Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'###  Created By                    :  Saurabh Verma

'#########################################################################################################################


Function saveJNLP(testEnvorniment)
		
		'(mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
		



		If checkProcessExists("jp2launcher.exe") Then
			Call KillProcess("jp2launcher.exe")
			'Call KillProcess("putty.exe")
			
		End If
		
		If checkProcessExists("putty.exe") Then
			'Call KillProcess("jp2launcher.exe") iexplore.exe
			Call KillProcess("putty.exe")
			
		End If
		

		If checkProcessExists("iexplore.exe") Then
			'Call KillProcess("jp2launcher.exe")
			Call KillProcess("iexplore.exe")
			
		End If
		
		
		
	   Select Case (trim(ucase(testEnvorniment)))
   
   					
			 	 Case "MTQA"
			 	 			ENVIRONMENT.LoadFromFile("C:\FabGUISmoke\testData\MTQA.XML")
			 	 			hostNAME=ENVIRONMENT.Value("hostName")
			 		  		fabURL = "http://"&hostNAME&":8088/fabgui/Loader?CallMode=WebStart"
		 	 			
			 	Case "DEV"
			 	 			ENVIRONMENT.LoadFromFile("C:\FabGUISmoke\testData\DEV.XML")
			 	 			hostNAME=ENVIRONMENT.Value("hostName")
			 		  		fabURL = "http://"&hostNAME&":8088/fabgui/Loader?CallMode=WebStart"
		 		  		 		  		
			 	

			 	 Case "FAB1"
			 	 
			 	 			
			 		  		ENVIRONMENT.LoadFromFile("C:\FabGUISmoke\testData\FAB1.XML")
			 		  		hostNAME=ENVIRONMENT.Value("hostName")
			 		  		fabURL = "http://"&hostNAME&":8088/fabgui/Loader?CallMode=WebStart"
'			 		  		folderPath= "C:\FabGUISmoke\config"
'			 		  		strAppPath = folderPath&"\Loader.jnlp"
			 	 
			 	 Case "FAB8"
			 	 
			 	 
			 				 ENVIRONMENT.LoadFromFile("C:\FabGUISmoke\testData\FAB8.XML")
			 				 hostNAME=ENVIRONMENT.Value("hostName")
			 				fabURL = "http://"&hostNAME&":8088/fabgui/Loader?CallMode=WebStart"
'			 		  		folderPath= "C:\FabGUISmoke\config"
'			 		  		strAppPath = folderPath&"\Loader.jnlp"
			 		  		
			 				 	 
	 End Select   		

'	Call DeleteFile(folderPath)
'	 		
'	 SystemUtil.Run "firefox.exe",fabURL
'	 
'	 Set oPage = Browser("CreationTime:=0").Dialog("regexpwndtitle:=Opening.*").Page("title:=Opening Loader.jnlp")
'	        	 
'	 If (oPage.Exist(20)) then 
'				
'				oPage.highlight
'				oPage.WebElement("innertext:=Save.*","html tag:=XUL:LABEL").Click
'				oPage.WebButton("type:=xul:button","name:=OK","html tag:=XUL:BUTTON").Click
'				wait(10)
'				Browser("CreationTime:=0").Close
'	 Else
'	
'				wait(2)
'		
'	End if 
'	
	saveJNLP = fabURL
'	
'	

		
End Function  


Function setRest(URL)
	 SystemUtil.Run "iexplore.exe",URL
	 Dialog("Index:=0").WinEdit("Index:=0").highlight
 	 Dialog("Index:=0").WinEdit("Index:=0").Set ENVIRONMENT.Value("restUName")
     Dialog("Index:=0").WinEdit("Index:=1").Set ENVIRONMENT.Value("restPassword")	
	Dialog("Index:=0").WinButton("text:=OK","nativeclass:=Button").Click
     wait(3)
     Browser("CreationTime:=0").Close
	
End Function

'########################################################################################################################
'###  Function Name                 :  startApplication
'###  Purpose                       :  This method is used to Launch the windows application
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################


Function startApplication(fabURL)
        
        						
        						UserName=GetLoggedInUserName()
        						strPath = "C:\Users\"&UserName&"\AppData\LocalLow\Sun"
								wait(10)
								Call DeleteFolder(strPath)
        						Dim objShell : Set objShell = CreateObject( "WScript.Shell" )
        						'msgbox fabURL
        						objShell.Run "javaws" & " "& fabURL, 7, FALSE
        						Call CloseJavaUpdateDialog()
        						If JavaWindow("title:=FabGUI.*").Exist(60) Then
        							startApplication = true
        						else
        							startApplication = false

        						End If
        						
        						
        						
       
End Function




'########################################################################################################################
'###  Function Name                 :  DeleteFolder
'###  Purpose                       :  This method is goign to delete fodler present in the path
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################

Function DeleteFolder(strFolderPath)
	Dim objFSO, objFolder
	Set objFSO = CreateObject ("Scripting.FileSystemObject")
	If objFSO.FolderExists(strFolderPath) Then
		objFSO.DeleteFolder strFolderPath, True
		DeleteFolder=True
	Else
       	DeleteFolder=False
	End If
	Set objFSO = Nothing
End Function

'########################################################################################################################
'###  Function Name                 :  Include
'###  Purpose                       :  This method  will include the Script object
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################
Function Include (Scriptnaam)
   Dim f, s, oFSO
    Set oFSO = CreateObject("Scripting.FileSystemObject")
    On Error Resume Next
    If oFSO.FileExists(sInstFile) Then
        Set f = oFSO.OpenTextFile(sInstFile)
        s = f.ReadAll
        f.Close
        ExecuteGlobal s
    End If
    On Error Goto 0
    Set f = Nothing
    Set oFSO = Nothing
End Function



'########################################################################################################################
'###  Function Name                 :  DeleteFile
'###  Purpose                       :  This method is goign to delete fodler present in the path
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################

Function DeleteFile(strFolderPath)
	Dim objFSO, objFolder
	Set objFSO = CreateObject ("Scripting.FileSystemObject")
	If objFSO.FolderExists(strFolderPath) Then
		If objFSO.FileExists(strFolderPath&"\Loader.jnlp") Then
			
			objFSO.DeleteFile (strFolderPath&"\*.jnlp"), True
			DeleteFile=True
			
		End If
	Else
       	DeleteFile=False
	End If
	Set objFSO = Nothing
End Function


'Function to get Logged In UserName
'########################################################################################################################
'###  Function Name                 :  GetLoggedInUserName
'###  Purpose                       :  This method is used to get lgoged userName
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################

Function GetLoggedInUserName()
	Set objNetwork = CreateObject("Wscript.Network")
	GetLoggedInUserName = objNetwork.UserName
End Function

'########################################################################################################################
'###  Function Name                 :  CloseJavaUpdateDialog
'###  Purpose                       :  This method is used to close the JavaUpdate Dialog 
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################
Function CloseJavaUpdateDialog()
	Dim objJavaUpdateDialog : Set objJavaUpdateDialog = Dialog("regexpwndtitle:=Java.*").WinButton("text:=Later")
	If objJavaUpdateDialog.Exist(5) Then
		objJavaUpdateDialog.Click
	End If
	Set objJavaUpdateDialog =Nothing
End Function

'########################################################################################################################
'###  Function Name                 :       fnScreenshot
'###  Purpose                       :       Capture ScreenShot
'###  Pre-Condition                 :       
'###  Created By                    :       Saurabh Verma
'#########################################################################################################################
Function fnScreenshot(testStep)
	
	  On Error Resume Next
 	 ScreenName = " "
     CurrentTime =Day(Now)&"_"& Month(Now)&"_"& Year(Now)&"_"& Hour(Now)&"_"& Minute(Now)&"_"& Second(Now)
     ScreenShotName = testStep &  CurrentTime & ".png"
     ScreenName ="C:\FabGUISmoke\screenShot"&"\"&ScreenShotName
      Desktop.CaptureBitmap ScreenName,True
      fnScreenshot =ScreenName
End Function



'########################################################################################################################
'###  Function Name                 :  checkProcessExists
'###  Purpose                       :  This method is used check if process exists
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################

Function checkProcessExists(processName)
	Dim strComputer,strProcess
	strComputer = "." ' local computer
	strProcess = processName '"jp2launcher.exe"
	' Check if Application  is running on specified computer (. = local computer)
	if isProcessRunning(strComputer,strProcess) then
		checkProcessExists = True
	else
		checkProcessExists = False
	end if	
End Function

' Function to check if a process is running
Function isProcessRunning(byval strComputer,byval strProcessName)

	Dim objWMIService, strWMIQuery

	strWMIQuery = "Select * from Win32_Process where name like '" & strProcessName & "'"
	
	Set objWMIService = GetObject("winmgmts:" _
		& "{impersonationLevel=impersonate}!\\" _ 
			& strComputer & "\root\cimv2") 
	if objWMIService.ExecQuery(strWMIQuery).Count > 0 then
		isProcessRunning = true
	else
		isProcessRunning = false
	end if

End Function

'########################################################################################################################
'###  Function Name                 :  CloseApplication
'###  Purpose                       :  This method is used for Closing Application
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################

Function CloseApplication()
	Dim UserName,strPath,result
	result = false
	blnKillProcess = KillProcess("jp2launcher.exe")
	if(blnKillProcess = true) then
		UserName=GetLoggedInUserName()
		strPath = "C:\Users\"&UserName&"\AppData\LocalLow\Sun"
		wait(30)
		blnActionFlag=DeleteFolder(strPath)
		If(blnActionFlag) Then
			Reporter.ReportEvent micPass , "Successfully Closed ", "PASS"
			result = true
		Else
			Reporter.ReportEvent micFail,"Unable To Close" ,"FAIL"
		End If
	End If
	CloseApplication = result
End Function

'Function to kill process in task manager
'########################################################################################################################
'###  Function Name                 :  KillProcess
'###  Purpose                       :  This method is used for kill favGUI Application
'###  Pre-Condition                 :       
'###  Created By                    :  Saurabh Verma
'#########################################################################################################################

Function KillProcess(nameofProcess)
           strComputer = "." ' local computer
	       strProcessName=nameofProcess'"jp2launcher.exe"
	       result = false
		If(isProcessRunning(strComputer,strProcessName)) Then
			SystemUtil.CloseProcessByName(strProcessName)
			Reporter.ReportEvent micPass , "Successfully Killed the Process "&strProcessName ,"PASS"
			result = true
		End If
		
		if(Err.Number > 0) Then
			Reporter.ReportEvent micFail , "Not able to Killed the Process "&strProcessName ,"FAIL"
			result = false
		End If
	KillProcess = result
End Function