﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1130935
'Description:Verify the Space Chart and Space MPC Chart Links are displayed for the inhibit.

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If

'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> "Inhibit" link.

	clickSiViewControlLink
	clickSiViewControlInhibit


'Step 12: Click on "Inhibit" link

		inhibitButton "InhibitList"
		
'Step 13: Enter the recipe id (which was inhibited above) in "recipe" field and owner sverma click on "Search" button.
	
	
	editBoxInhibit "Register User" , ENVIRONMENT.Value("SPACEInhibit")
	inhibitButton "Search Inhibits"
	waitInhibitTable

'Step 14: Verify the "Registered Comments" column in the data table.

		actualResult=verifyRegiterComment(getDataFromInhibitListTable (1,6))
		
	If actualResult=1 Then
		
		Reporter.ReportEvent micPass ," The SPACE link is having CHK_ID, SAMPLE_ID in the comment","Passed"
	else
	
		Reporter.ReportEvent micFail ," The SPACE link is NOT having CHK_ID, SAMPLE_ID in the comment","Failed"

	End If	
		

''Step 8: Close fabGUI
	
CloseApplication
