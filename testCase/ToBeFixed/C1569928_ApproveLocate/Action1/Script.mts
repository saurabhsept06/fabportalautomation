﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1569928
'Description:Verify the Space Chart and Space MPC Chart Links are displayed for the inhibit.

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	setApproveLocate (testENV)
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If

'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> "ApproveLocate" link.

	clickSiViewControlLink
	clickApproveLocate


'Step 4: Copy &amp; paste / enter the lot id in Lot Id field. Lot Id

	approveLocateButton"Overview"
	selectListApproveLocate "Lot Id",ENVIRONMENT.Value("ApproveLocateLotID")
	approveLocateButton "Locate Forward"
	wait(2)
	selectListApproveLocate "Oper ID.*","#2"
	approveLocateButton "New.*"
 @@ hightlight id_;_1997613595_;_script infofile_;_ZIP::ssf58.xml_;_
 ' Step 5: Click on New Request and fill the details
 
 	newRequest "#2","This is automation Approve"
 	
 	approveLocateTextBox "Lot Id", ENVIRONMENT.Value("ApproveLocateLotID")
 	
 	approveLocateButton "Filter"


'Step 6: Verify the "Lot Id" column. and Direction it should be forward and Lot ID should be same 

	lotID = getApproveLocateTableValue( 0,1)
	
	
	
	direction = getApproveLocateTableValue( 0,2 )
	
	If (lotID=ENVIRONMENT.Value("ApproveLocateLotID")) Then
		
		reporter.ReportEvent micPass, "The Approve locate is set" ,"Passed"
	
	else
	
		reporter.ReportEvent micFail, "Approve locate is not set ", "Failed"
		
	End If
	
	If (trim(direction)="Forward") Then
		
		reporter.ReportEvent micPass, "The Approve locate is set" ,"Passed"
	
	else
	
		reporter.ReportEvent micFail, "Approve locate is not set ", "Failed"
		
	End If



'Step 9: tear down 

	selectApproveLocateRow 0, 0
	approveLocateButton "Cancel.*"
	 
	cancelRequest "Cancelling the request"

	
	CloseApplication

