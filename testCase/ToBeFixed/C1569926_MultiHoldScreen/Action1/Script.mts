﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1130935
'Description:Verify the Space Chart and Space MPC Chart Links are displayed for the inhibit.

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	cancelprocessHold ENVIRONMENT.Value("env") ,ENVIRONMENT.Value("ProcessHoldRouteID")
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If

'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> "MultiHold" link.

	clickSiViewControlLink
	clickMultiHold

'Step4: Click on Process Hold button 
	multiHoldutton "Process Hold"
	waitProcessHoldPage

'Step 5: Select the Not Hold checkbox under Execute Hold for WIP Lots on the selected operation and click on "Search" button.

	selectListProcessHold "MainPD" ,ENVIRONMENT.Value("ProcessHoldmainPD")
	selectListProcessHold "Operation ID" ,ENVIRONMENT.Value("ProcessHoldOperID")
	multiHoldutton "Search"
	waitforFirstTableProcessHold
	selectRowFromTableProcessHold 0,0

'Step 6: Click on "Click on Add Route specific PH " 
	
	multiHoldutton "Add Route specific PH"
	waitforFirstTableProcessHold
	selectRowFromTableProcessHoldSecond 0,0
	
'Step 7: Set Reason Code for the hold. Click on "Set ProcessHold" button	

	multiHoldutton "Set ReasonCode/ClaimMemo"
	
	setReasonCodePOPUP "AERF" , "This is comment for processHold from automation"
	
	multiHoldutton "Set ProcessHold"
	
'Step 8: 	Click on "Close" button.


	If (instr(setProcessHold,"Successfully")>0) Then
	
		Reporter.ReportEvent micPass, "The MultiHold functionality is working" ,"PasseD"
	else
	
		Reporter.ReportEvent micFail, "The MultiHold functionality is NOT working" ,"Failed"
		
	End If 

'Step 9:
	cancelprocessHold ENVIRONMENT.Value("env") ,ENVIRONMENT.Value("ProcessHoldRouteID")	
	CloseApplication


