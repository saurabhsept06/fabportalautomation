﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1229104
'Description:Verify the Script parameter functionality

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	toClearERPItem ENVIRONMENT.Value("env"),ENVIRONMENT.Value("ScriptParaMeterLOT")
		
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If

'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> "Script Parameter" link.

	clickSiViewControlLink
	clickSiViewControlScriptParamter

'Step 4: Select type as Lot from type drop-down box, enter lot id in Lot ID field and click on show button

selectListinScriptParaMeter "Type","Lot"

selectListinScriptParaMeter "Name","ERPItem"

editBoxScriptParamter "firstEditBox",ENVIRONMENT.Value("ScriptParaMeterLOT")

ScriptParameterButton "Show"


'Step 4: Enter the value in ERP ITem ScriptParaMeterERPITEM

 selectRowFromTable 0,0
 
setValueinTable 0,8,ENVIRONMENT.Value("ScriptParaMeterERPITEM")

ScriptParameterDialogBox "OK"


'Step 5:Verify the New Value field.

actualResult =getvalueFromTable (0,7)

If actualResult= ENVIRONMENT.Value("ScriptParaMeterERPITEM") Then
	
	reporter.ReportEvent micPass ," The Script Parameter functionality is working" ,"Passed"
	
else

	reporter.ReportEvent micFail ," Script ParaMeter fucntionality is not working" ,"Failed"
End If

''Tear down'
toClearERPItem ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotPriorityLodID")

'Step 8: Close fabGUI
CloseApplication


 @@ hightlight id_;_512903389_;_script infofile_;_ZIP::ssf42.xml_;_