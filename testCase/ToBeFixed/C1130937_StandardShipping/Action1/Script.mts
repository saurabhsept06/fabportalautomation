﻿'testCaseURL: http://vlfcrailsp01/testrail/index.php?/cases/view/1130937
'Description:Verify IntraFab

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	
	testENV= readFile
	getLoader = saveJNLP(trim(testENV))
	setconfig(testENV)
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	' Take lot out of bank 
	
		bankinCancel ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID")
	'Clear EPR item 
		toClearERPItem ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID")
			
	'Set userParatmeter	ERP item , turn Key 
	
		
		setuserParameter ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID"),"TurnkeyType","U"
		setuserParameter ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID"),"ERPItem",ENVIRONMENT.Value("STDERPITEM")
		setuserParameter ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID"),"QualityGateApproval",ENVIRONMENT.Value("STDqualityGate")
		
		
		
	'locate lot
		
			locateLOT ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID"),ENVIRONMENT.Value("STDOperationID")		
	
	
	
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If


'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on Shipping>Shipment

	clickShippingMainButton
	shippingButton "Shipment"

'Step 4: Select Lot typ from list and enter lot ID 


	ShippingListBox "Type" , "Lot ID"
	
	ShippingListBox "Value",ENVIRONMENT.Value("STDLotID")
	shippingButton "Add.*"
	
	waitForTableShipping

'Step 5: Click on Sales Order Shipment Link

	shippingButton "Sales.*"	
	waitForTableShipping
	
'Step 6:Select a row from the data table which has Lot ID which you created in pre-condition and click on Show Lot
	selectRowinSalesOrdeShipement(ENVIRONMENT.Value("STDERPITEM"))
	shippingButton "Show.*"

'Step 7: click on Attach to SO and Move to FG-Bank button.

	waitForTableShipping
	selectRowinSalesOrdeShipementDetail(ENVIRONMENT.Value("STDLotID"))
	shippingButton "Attach.*"
	clickMessage
	clickMessage
	waitforLotShipping
'Step 8: Check status of the Lot 

	actualResult =getLotInfo (ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID"))
	'msgbox actualResult
	If (instr(actualResult,"COMPLETED")>0)Then
		
		Reporter.ReportEvent micPass," Shipping functionality is working", "Passed"
		
	else
	
		Reporter.ReportEvent micFail," Shipping functionality is NOT working", "Failed"
		
	End If
	
	
'Close Application

'tear Down 
' Take lot out of bank 
	bankinCancel ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID")
	'Clear EPR item 
		toClearERPItem ENVIRONMENT.Value("env"),ENVIRONMENT.Value("STDLotID")

CloseApplication