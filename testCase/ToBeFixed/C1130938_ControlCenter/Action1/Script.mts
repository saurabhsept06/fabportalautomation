﻿'testCaseURL: http://vlfcrailsp01/testrail/index.php?/cases/view/1130938
'Description:Verify Control Center

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	
	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If


'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on Control Center link.

	controlCenterButton "Control.*"
	controlCenterButton "EI-S.*"
	wait(2)
	controlCentertxtBox "Entity",ENVIRONMENT.Value("Entity")
	
	controlCenterButton "Show"
	

'Step 4: Enter the valid tool name in ?Entity? text box. 


	addToFavorite

	actualResult= checkFavoriteTree (ENVIRONMENT.Value("Entity"))
	 If actualResult Then
	 	reporter.ReportEvent micPass ,"The Tool is added to favorite" ,"Passed"
	 else
	 
	 	Reporter.ReportEvent micFail, "The Toold is not added " , "Failed"
	 	
	 End If
	

	
'Close Application

 CloseApplication







JavaWindow("FabGUI Portal").JavaTab("Lot Actions").Select "NonStdShipment" @@ hightlight id_;_451586166_;_script infofile_;_ZIP::ssf25.xml_;_
JavaWindow("FabGUI Portal").JavaTab("Lot Actions").Select "OtherActions" @@ hightlight id_;_451586166_;_script infofile_;_ZIP::ssf26.xml_;_