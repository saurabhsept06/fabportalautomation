﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1569929
'Description:Verify IntraFab

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	
	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If


'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> IntraFab

	clickSiViewControlLink
	clickSiViewControlIntraFabMove
	
'Step 4: Enter Lot id in the text Box 

	intraFabEditBox "Lot.*",ENVIRONMENT.Value("IntraFabLodID")

'Step 5: Click on button 

	intraFabButton "Add.*"
	
'Step 6: Select an option from "To Facility" drop-down list, enter comments in "Claim Memo" field and click on "Save" button
	
	wait(1)
	setCellDataIntraFab 0,4
	selectRowFromTableIntraFab 0, 0
	intraFabEditBox "Claim.*","Calim Memo for  intraFab-Move usign Automation"
	
	intraFabButton "Save"
	
		
	If (verifyData( 0,3)) Then
		
		Reporter.ReportEvent micPass ,"The Intra-Fab functionality is working" ,"Passed"
	else
	
		Reporter.ReportEvent micFail , "The Intra-Fab functionality is NOT working" ,"Faied"
	End If
	
	
	
'Close Application

CloseApplication

