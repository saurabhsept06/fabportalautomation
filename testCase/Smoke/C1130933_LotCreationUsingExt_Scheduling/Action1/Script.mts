﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1130933
'Description:This test case is to verify 
'				Creation of Lot using Ext. scheduling
'				Functionality verified: Creation of Lot using Ext. Scheduling

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 
	testENV= readFile
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
		
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		 ExitAction 
		
	End If

'Step 2: Log-in to Fab GUI application using valid user credentials.
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	verifyLogin
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
'Step 3: Click on SiView Control link and then Scheduler

	clickSiViewControlLink
	clickSiViewControlScheduler
	
	Reporter.ReportEvent micPass , "Step 3: " , "Click on SiView Control link. - Passed "

'Step 4: Click on Lot Scehduler button and verify that page displayed 
	
		clickButtonLotScheduler()
		verifyDevelopmentLotSchedulingPanel
	
	Reporter.ReportEvent micPass , "Step 4: " , "Click on Lot Scehduler button and verify that page displayed - Passed "


 @@ hightlight id_;_159898591_;_script infofile_;_ZIP::ssf36.xml_;_
'Step 5: Select product from "Product ID" , "Customer Code"  "SubLot Type =PC", "Target",Select "Lot Owner" 


	setValueLotScheduling
	
'Step 6: Lot id field is updated with the generated LotID
 @@ hightlight id_;_1254889588_;_script infofile_;_ZIP::ssf43.xml_;_
 lotID=verifyLotScheduling
 
 'Step 7 verify in siView 
 
 siViewResult= executeRubyToCheckLotisSTB(ENVIRONMENT.Value("env"),lotID)
 
 If (instr(siViewResult,"SiView::MM::ReleasedLot"))>0 Then
 	
 	reporter.ReportEvent micPass , "Passed" ,"Lot is created for sTB"

 End If
 
  If (instr(siViewResult,"no request to release a product."))>0 Then
 	
 	reporter.ReportEvent micFail , "Failed" ,"Lot is created for sTB"

 End If
 
 
CloseApplication
KillProcess("putty.exe")