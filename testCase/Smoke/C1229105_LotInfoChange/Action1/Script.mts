﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1229105
'Description:Verify the  Lot info Change

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	testENV= readFile
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If


'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	toReleaseLotHold ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotInfoChangeLotID"),ENVIRONMENT.Value("lotInfoChangeholdType")
	tochangeSubLotType ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotInfoChangeLotID"),ENVIRONMENT.Value("lotInfoChangesublotType")
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> "Lot Info Change" link.

	clickSiViewControlLink
	clickLotInfoChange

'Step 4: Change "Search Type" to 'LOt ID', enter "LOT ID" and verify the Search button.


	selectListinLotSelection "Search Type", "LotID"
	
	stypeinEditBox "LotID",ENVIRONMENT.Value("lotInfoChangePageLotID")
	
	lotInfoChangeButton("Search")
	
	
'Step 5: Select the lot from the data table and click on "Change Lot Information" button.
	
		selectRowFromTable 0,0
		
		lotInfoChangeButton "Change Lot Information"
		
 @@ hightlight id_;_855361412_;_script infofile_;_ZIP::ssf32.xml_;_
'Step 6: Change the subLotTYpe, enter ERPItem and click on Execute button.

		selectListinLotSelection "SublotType" ,"PO"
		
		selectListinLotSelection "ERPItem" ,"TESTAUTOMATION"
		
		lotInfoChangeButton "Execute"
		
		claimMemoDialogBx
		
		setERPItem = getvalueFromTable(1,14)
		
		'msgbox setERPItem
		If (instr(setERPItem,"TESTAUTOMATION"))>0 Then
			
			reporter.ReportEvent micPass, "ERP item is set for the lot" ,"Passed"
			
			else
			
			reporter.ReportEvent micFail, "ERP item is NOT set for the lot" ,"Failed"
		End If
		
		
'Step 7: Once the process is completed, verify the message

verifyChangeLotInfoMessage

'Tear down'

tochangeSubLotType ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotInfoChangeLotID"),ENVIRONMENT.Value("lotInfoChangesublotType")
toClearERPItem ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotInfoChangeLotID")
toReleaseLotHold ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotInfoChangeLotID"),ENVIRONMENT.Value("lotInfoChangeholdType")

'Step 8: Close fabGUI
CloseApplication
