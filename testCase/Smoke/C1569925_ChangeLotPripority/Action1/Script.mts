﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1569925
'Description:Verify the Change Lot Priority screen

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	toClearERPItem ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotPriorityLodID")
		
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If

'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> "Lot Info Change" link.

	clickSiViewControlLink
	clickLotInfoChange

'Step 4: Change "Search Type" to 'LOt ID', enter "LOT ID" and verify the Search button.


	selectListinLotSelection "Search Type", "LotID"
	
	stypeinEditBox "LotID", ENVIRONMENT.Value("lotPriorityLodID")
	
	lotInfoChangeButton("Search")
	
	
'Step 5: Select the lot from the data table and click on "Change Lot Priority" button.
	
		selectRowFromTable 0,0
		
		lotInfoChangeButton "Change Lot Priority" 
		
'Step 6: Select/Change the priority by selecting from "Customer Priority" drop-down list.

			
		selectListinLotSelection "Customer Priority" ,"SRKT"
		
		custPriorityBeforeExecution = getvalueFromTable(1,9)

'Step 7: Select/Change the priority by selecting from "TST Priority" drop-down list.

			
		selectListinLotSelection "TST Priority" ,"1: Rocket"
		tstPriorityBeforeExecution = getvalueFromTable(1 ,11)
 @@ hightlight id_;_900551901_;_script infofile_;_ZIP::ssf29.xml_;_
 'Step 8: Select/Change the priority by selecting from "TST Priority" drop-down list.


		lotInfoChangeButton "Execute"
		
		claimMemoDialogBx
		
		
		
	'Step 9: Once the process is completed, verify the messages.	
	
		verifyChangeLotInfoMessage
 
 
 
 custPriorityAfterExecution = getvalueFromTable(0 ,9)
 tstPriorityAfterExecution = getvalueFromTable(0 ,11)
 
 

 
 If (instr(custPriorityBeforeExecution,custPriorityAfterExecution))>0  Then
 	
 	 reporter.ReportEvent micPass ," The Customer priority changed","PasseD"
 else
 
 	reporter.ReportEvent micFail ,"  The Customer priority NOT changed","Failed"
 
 End If
 

'Tear down'
toClearERPItem ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotPriorityLodID")
toReleaseLotHold ENVIRONMENT.Value("env"),ENVIRONMENT.Value("lotPriorityLodID"),ENVIRONMENT.Value("lotInfoChangeholdType")

'Step 8: Close fabGUI
CloseApplication





 @@ hightlight id_;_512903389_;_script infofile_;_ZIP::ssf42.xml_;_