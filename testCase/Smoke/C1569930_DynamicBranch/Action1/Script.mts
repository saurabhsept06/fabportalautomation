﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1569930
'Description:Verify IntraFab

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	
	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	cancelDynamicBranch ENVIRONMENT.Value("env"),ENVIRONMENT.Value("DynamicLotID")
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If


'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> Dynamic Branch

	clickSiViewControlLink
	clickSiViewControlDB
	
'Step 4: Enter Lot id in the text Box 

	editBoxdynamicBranch "Lot",ENVIRONMENT.Value("DynamicLotID")
	
'Step 5 : Click on Search button 


	 dynamicBranchButton "Search"
	 wait(1)
	 
'Step 6: Select route id from Branch To Route drop-down list, enter comments in Claim Memo field and verify the Execute Dynamic Branch button.

	selectListdynamicBranch "#0"
	
	editBoxdynamicBranch "Claim", "This is claim memo for the Dynamic Branch usign Automation"
	
	 dynamicBranchButton "Exec.*"
 @@ hightlight id_;_2000705292_;_script infofile_;_ZIP::ssf25.xml_;_
 
 If (instr(verificationmessage ,"Information"))>0 Then
 	 
 	 
 	 Reporter.ReportEvent  micPass ," The Dynamic Branch functionalist working" ,"Passed"
 	 
 else
 
 		Reporter.ReportEvent  micFail ," The Dynamic Branch functionalist not working" ,"Failed"
 	
 End If 
	
'Close Application

CloseApplication

