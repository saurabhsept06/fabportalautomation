﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1130934
'Description:Verify the Inhibit creation  functionality

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	cancelInhibitRecipe ENVIRONMENT.Value("env"),ENVIRONMENT.Value("inhibitRecipe")	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If

'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> "Inhibit" link.

	clickSiViewControlLink
	clickSiViewControlInhibit

'Step 4: Click on Recipe MAtrix Link 

		inhibitButton "RecipeMatrix"
		
		
'Step 5: Enter "Equipment" (PK-EQP006) and press "Search Inhibits" button.

		editBoxInhibit "Equipment",ENVIRONMENT.Value("inhibitEQP")
		inhibitButton "Search Inhibits"
		waitLoadingRecipeMatrix

'Steo 6: Use right mouse button and drag mouse over the cells.

		setTableCell 1,0
		
		
		
'Step 7: Check the + sign

		actualData= getvalueFromTableInhibit(1,0)
		
		If actualData = "+" Then
			Reporter.ReportEvent micPass ,"The inhibit was setand + sign is seen" , "Passed"
		else
			Reporter.ReportEvent micFail ,"The inhibit was setand + sign is NOT seen" , "Fail"
		End If
		
'Step 8: Select SubLotTypes and Reason Code.
checkBoxInhibit "All SubLotTypes" ,"ON" @@ hightlight id_;_327612028_;_script infofile_;_ZIP::ssf43.xml_;_

selectListInhibit "ReasonCode",ENVIRONMENT.Value("inhibitReasonCode")


'Step 9: Press "Create Inhibits" button.

	inhibitButton "Create Inhibits"
 @@ hightlight id_;_945276362_;_script infofile_;_ZIP::ssf50.xml_;_
'Step 10: nter comments in Claim Memo field, select Inhibit Owner, estimated release time and click on OK button.

	enterTextInDialogBox "This is from Automation"
	
	confirmation
	
' Step 11: Wait till inhibti get created 

		waitForProgres

'Step 12: Click on "Inhibit" link

		inhibitButton "InhibitList"
		
'Step 13: Enter the recipe id (which was inhibited above) in "recipe" field and owner sverma click on "Search" button.
	
	editBoxInhibit "Recipe" , ENVIRONMENT.Value("inhibitRecipe")
	editBoxInhibit "Register User" , ENVIRONMENT.Value("uName")
	inhibitButton "Search Inhibits"
	waitInhibitTable

'Step 14: verify inhibit created 

	actualResult=verifyInhibitMessage

	 If (instr(actualResult,"found"))>0 Then
	 	
	 	reporter.ReportEvent micPass ," Inhibit was created and verified in inhibit list", "Passed"
	 else
	 
	 	reporter.ReportEvent micFail ," Inhibit was NOT created and verified in inhibit list", "Failed"
	 	
	 End If


''Step 8: Close fabGUI
cancelInhibitRecipe ENVIRONMENT.Value("env"),ENVIRONMENT.Value("inhibitRecipe")		
CloseApplication


