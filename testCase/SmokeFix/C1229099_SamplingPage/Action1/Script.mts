﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1229099
'Description:Verify that Sampling request can be sent via FabGUI

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma




'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	
	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If


'Step 2: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	login
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	
	
'Step 3: Click on SiView Control> Sampling

	clickSiViewControlLink
	clickSiViewControlSampling
	
	
'Step 4: Under Wafer Sampling provide LotID and click on Load Lot(s)

	selectWaferSamplingRadio "Carrier" 

	SamplingTextBox "Carrier",ENVIRONMENT.Value("CarrierID")

	SamplingButton "Load" 
	
'Step 5: 	Select the few wafers and select MUST (x) flag from "MultiEdit" drop-down list.

	waitForTable
	
	multipleRowSelection
	
	SamplingListBox "Reason", ENVIRONMENT.Value("ReasonCode")
	
	SamplingListBox "MultiEdit","x"
 
	
'Step 6: Enter the comment in User Comment field, reason code in Reason Code filed and click on Submit button.
	
	SamplingTextBox "Comment","This is Comment from Smoke Automation"
	
'Step 7: click on Submit button.

	SamplingButton "Submit" 
	wait(10)

'Step 8: Verify the Active Selection 


'msgbox instr(getActiveSelectionValue,"x")
	If (instr(getActiveSelectionValue,"x")>0) Then
		reporter.ReportEvent micPass ,"Active selection is X for sampling" ,"Passed"
	else
	
		reporter.ReportEvent micFail ,"Active selection is NOT X for sampling" ,"Failed"
		
	End If
	
'Tear Down 
	multipleRowSelection
	
	SamplingListBox "Reason",ENVIRONMENT.Value("ReasonCode")
	
	SamplingListBox "MultiEdit","-"
	SamplingButton "Submit" 
	wait(10)
	
'Close Application

CloseApplication
	
	
	