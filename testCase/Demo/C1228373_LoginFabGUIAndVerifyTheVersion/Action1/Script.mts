﻿'testCaseURL:http://vlfcrailsp01/testrail/index.php?/cases/view/1228373
'Description:This test case is to verify 
'				1. Login to FabGUI application 
'				2. Check the version of the application 
'				3. Logout from application

'Pre-Condition                 :  Environment(FAB1,FAB8,MTQA) and hostNAME (mtqa:fc8fguim02 ,fab1:f36fabguid2,fab8:fc8fguiq03)
									   ' FAB1: http://f36fabguid2:8088/fabgui/Loader?CallMode=WebStart
									   'MTQA: http://fc8fguim02:8088/fabgui/Loader?CallMode=WebStart
									   'FAB8: http://fc8fguiq03:8088/fabgui/Loader?CallMode=WebStart
									   'DEV : http://f36usvd3:8088/fabgui/Loader?CallMode=WebStart
									   'Set FireFox download to C:\FabGUISmoke\config
'Created By                    :  Saurabh Verma






'Percondition 1 : Save the jpnl file in 'TODO : Move the envorinment and hostname to excel or jason or XML 

	
	testENV= readFile
	
	getLoader = saveJNLP(trim(testENV))
	Reporter.ReportEvent micPass , "Percondition 1" , "The jpnl file is saved in \FabGUISmoke\config folder-Passed"
	
	
'Step 1: Launch the application 
	
	If startApplication(getLoader) Then
	
		Reporter.ReportEvent micPass , "Step 1" , "Launch the application "
		
	Else
	
		Reporter.ReportEvent micFail , "Step 1" , "Launch the application NOT Done "
		
	End If


'Step 2: Verify the BuildNumber
	
	verifyBuildNumber(ENVIRONMENT.Value("buildNumber"))
	Reporter.ReportEvent micPass , "Step 2: " , "Verify the BuildNumber"
	

'Step 3: Provide username and password in the textBox
	
	setuName(ENVIRONMENT.Value("uName")) 
	setPassword(ENVIRONMENT.Value("pPassword"))
	Reporter.ReportEvent micPass , "Step 2: " , "Provide username and password in the textBox - Passed "
	

'Step 4: Click on Login

	login
	verifyLogin
	Reporter.ReportEvent micPass , "Step 3: " , "Click on Login - Passed "
	
'Step 5: Close Application 

	CloseApplication
	
		