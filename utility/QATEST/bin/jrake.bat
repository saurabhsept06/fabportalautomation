@ECHO OFF
:: automatically generated script, make changes in Rakefile
pushd %~dp0
cd %~dp0..
if not defined java set java=java
for /F %%i in ('@dir /B lib\jruby-complete*jar') do set CP=lib\%%i
echo Starting JRake
if not title == '' title JRake
%java% -cp %CP%;etc -server -noverify -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC -Djava.awt.headless=true -XX:ReservedCodeCacheSize=128M -XX:MaxPermSize=256m -Djruby.jit.max=32768 -Djruby.jit.threshold=5 -Dcompile.invokedynamic=true org.jruby.Main -r ./ruby/gems.jar -S rake %*
popd

goto :EOF
::
:setcp
if defined CP (set CP=%CP%;%*) else (set CP=%*)
goto :EOF