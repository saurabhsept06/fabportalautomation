#!/bin/bash
# automatically generated script, make changes in Rakefile
pushd $(dirname $BASH_SOURCE)/..
export LANG=en_US.utf8
CP=$(ls lib/jruby-complete*jar|tail -n 1):etc
if [ "x$java" = "x" ]; then
  sys=$(uname -s)
  if [ $sys = "AIX" ]; then
    f=./java7/jre/bin/java
  elif [ $sys = "Linux" ]; then
    f=/3rd_party/java/Linux/jdk1.7.0_80_x64/bin/java
  fi
  test -f $f && java=$f
fi
java=${java:-java}
$java -cp $CP -server -noverify -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC -Djava.awt.headless=true -XX:ReservedCodeCacheSize=128M -XX:MaxPermSize=256m -Djruby.jit.max=32768 -Djruby.jit.threshold=5 -Dcompile.invokedynamic=true org.jruby.Main ./ruby/tcrunner/run.rb -r --env f8stag $*
popd
