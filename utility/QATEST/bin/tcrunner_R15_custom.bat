@ECHO OFF
:: automatically generated script, make changes in Rakefile
pushd %~dp0
cd %~dp0..
if not defined java set java=java
for /F %%i in ('@dir /B lib\jruby-complete*jar') do set CP=lib\%%i
echo Starting TCRunner R15_custom
if not title == '' title TCRunner R15_custom
%java% -cp %CP%;etc -server -noverify -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC -Djava.awt.headless=true -XX:ReservedCodeCacheSize=128M -XX:MaxPermSize=256m -Djruby.jit.max=32768 -Djruby.jit.threshold=5 -Dcompile.invokedynamic=true org.jruby.Main ./ruby/tcrunner/run.rb -r --env f8stag %*
popd

goto :EOF
::
:setcp
if defined CP (set CP=%CP%;%*) else (set CP=%*)
goto :EOF