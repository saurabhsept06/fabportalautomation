# GLOBALFOUNDRIES Proprietary / GLOBALFOUNDRIES Confidential
# Copyright 2007-2010 Advanced Micro Devices
# Copyright 2010 GLOBALFOUNDRIES
#
# $Id: Sampling.Skip.tcl 97817 2012-09-21 19:36:30Z lchee $
# $HeadURL: https://gfsvnp01/svn/apc/scripts/trunk/PCL/Application/Sampling/Sampling.Skip.tcl $
#
# Role:      Sampling Skip
#            organizes the local sampling skip
#
# Author: Robert Barlovic
#
set sScriptName "Sampling.Skip.tcl"
set aScriptVersions($sScriptName) [lindex {$Revision: 97817 $} 1]
$::oAmdAppLog writeInfo "Running $sScriptName - version $aScriptVersions($sScriptName)"

lappend ::lControllers "SamplingSkip"

UtilityBaseline::RegisterUserCleanUpAndExitHandler SamplingSkip::Report

namespace eval SamplingSkip {

    # AH entries
    variable aHistoryEntriesSkip
    variable aHistoryEntriesStay
    variable aHistoryEntriesReason

    # internal params
    variable aSkipParameter

    # support for lean script
    proc run {} {

        $::oAmdAppLog writeTrace "in run" 
        ProcessMetrologyData

    }

    #
    # @desc   do the local sampling skip
    #
    proc ProcessMetrologyData {} {

        #  aSiViewSkipResults is evaluated by CreateJobResultsDictionarySiView based on originatorID
        global aSiViewSkipResults
        global aRunContext
        set aSiViewSkipResults(sReturn) ""
        set aSiViewSkipResults(sReturn_Fail) ""

        #
        # pre checks
        #

        # check job mode
        if {[PCLRuntimeData::GetJobMode] != "PCLSpecsPRE1"} {
            set aSiViewSkipResults(sReturn_Fail) "Wrong JobMode"
            PCLJobStart::FailureDueTo "WrongJobMode"
        }

        # check lotID
        set sLotID [PCLRuntimeData::MaterialContext::GetLotID]
        if {$sLotID == ""} {
            set aSiViewSkipResults(sReturn_Fail) "Could not extract LotID"
            PCLJobStart::FailureDueTo "LotID" "SAMPLING_1"
        }

        # check operationID
        set sOperationID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "operationID" $sLotID]
        if {$sOperationID == ""} {
            set aSiViewSkipResults(sReturn_Fail) "Could not extract OperationID"
            PCLJobStart::FailureDueTo "OperationID" "SAMPLING_3"
        }

        # check operationNumber
        set nOperationNumber [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "operationNumber" $sLotID]
        if {$nOperationNumber == ""} {
            set aSiViewSkipResults(sReturn_Fail) "Could not extract OperationNumber"
            PCLJobStart::FailureDueTo "OperationNumber" "SAMPLING_3"
        }

        # check routeID
        set sRouteID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "routeID" $sLotID]
        if {$sRouteID == ""} {
            set aSiViewSkipResults(sReturn_Fail) "Could not extract RouteID"
            PCLJobStart::FailureDueTo "OperationNumber" "SAMPLING_3"
        }

        # check mandatoryFlag
        set bMandatoryOperationFlag [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "mandatoryOperationFlag" $sLotID]
        if {$bMandatoryOperationFlag == ""} {
            set aSiViewSkipResults(sReturn_Fail) "Could not extract MandatoryOperationFlag"
            PCLJobStart::FailureDueTo "MandatoryOperationFlag" "SAMPLING_3"
        }

        # get department, just for email therefore no check
        set sDepartment [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "department" $sLotID]

        # check nIndex
        set nIndex [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "nIndex" $sLotID]
        if {$nIndex == ""} {
            set aSiViewSkipResults(sReturn_Fail) "Could not extract Index"
            PCLJobStart::FailureDueTo "Index" "SAMPLING_3"
        }
        $::oAmdAppLog writeWarning "-- SamplingSkip::ProcessMetrologyData: CurrentPD located at $nIndex"

        #
        # darling you gotta let me know should i stay or should i skip ...
        #

        # some initilization
        set sPSM ""
        set lWaferSelection ""
        set nSelectedWaferCount 0
        set bManualSelection 0
        set sSelection ""
        set sSelectedWaferCount "WCNT=00"
        set sResultCode "SKIPFAIL"

        # initialize config
        PCLRuntimeData::DOM::InitializeConfig
        set _Config_ [PCLRuntimeData::DOM::GetConfig]
        set _ConfigSchema_ [PCLRuntimeData::DOM::GetConfigSchema]

        if {$bMandatoryOperationFlag != 0} {
                
                set sResultCode "STAY"
                $::oAmdAppLog writeTrace "mandatory"

        } else {

            # check wafer scribes
            set lWaferID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "lWafer,waferID" $sLotID]
            if {$lWaferID == ""} {
                set sMsg "Could not extract WaferScribes"
                $::oAmdAppLog writeWarning "-- SamplingSkip::ProcessMetrologyData: $sMsg"
                set aSiViewSkipResults(sReturn_Fail) $sMsg
                PCLJobStart::FailureDueTo "WaferScribes" "SAMPLING_3"
            }

            # check process layer
            set sProcessLayer [PCLRuntimeData::MaterialContext::GetGenericPDUData "sProcessLayer" $sLotID]
            if {$sProcessLayer == ""} {
                set sMsg "Could not extract ProcessLayer"
                $::oAmdAppLog writeWarning "-- SamplingSkip::ProcessMetrologyData: $sMsg"
                set aSiViewSkipResults(sReturn_Fail) $sMsg
                PCLJobStart::FailureDueTo "ProcessLayer" "SAMPLING_12"
            }

            # check process type
            set sProcessType [PCLRuntimeData::MaterialContext::GetGenericPDUData "sProcessTypeWfrSelection" $sLotID]
            if {$sProcessType == ""} {
                set sMsg "Could not extract ProcessType"
                $::oAmdAppLog writeWarning "-- SamplingSkip::ProcessMetrologyData: $sMsg"
                set aSiViewSkipResults(sReturn_Fail) $sMsg
                PCLJobStart::FailureDueTo "ProcessType" "SAMPLING_12"
            }

            # assume, we have no seleced wafer, but we want to stay, except we have a special run context set
            set sResultCode "STAY"
            if {[info exists aRunContext(apcTestSkip)]} {
                set sResultCode "SKIP"
            }

            # check for exception PD's, SKIP as well as STAY. these are not metrology PD's, therefore no check if wafers selected at all
            array unset aParameter
            set aParameter(Type) "ListSampling"
            set sSpecID [XMLHandling::XML.Get $_ConfigSchema_ $_Config_ "Configuration" "SpecSpecialSpecIDByType" [array get aParameter]]
            if {$sSpecID != ""} {
                set sExceptionCode [PCLDataCollection::apcLotData::GetListSamplingValue $sSpecID "EXCEPTION" "DEFAULT" $sOperationID]
                if {($sExceptionCode == "STAY")||($sExceptionCode == "SKIP")} {
                    $::oAmdAppLog writeWarning "-- SamplingSkip::ProcessMetrologyData: $sExceptionCode since $sOperationID is an Exception PD"
                    set sResultCode $sExceptionCode
                }
            }

            # check for PSM -> has to be checked since gate pass possible if split PD not mandatory -> not covered by SiView
            # simply stay in the PD to execute the PSM
            if {$sResultCode == "SKIP"} {
                set sPSM [CheckPSM]
                if {$sPSM == "SKIPFAIL"} {
                    set sResultCode "SKIPFAIL"
                } elseif {$sPSM == "PSM"} {
                    set sResultCode "STAY"
                }
            }

            # check for ProcessHold -> no check needed in local skip since:
            #   - if Process Hold set the lot goes on hold during skip before pre1 execution at the PD - hold release leads to script execution

            # check for FutureHold -> no check needed in local skip since:
            #   - if pre1 FH the lot goes on hold during skip before pre1 execution at the PD - hold release leads to script execution
            #   - if post FH the lot goes on hold on the following PD - hold release leads to script execution

            

        }

        if {[info exists aRunContext(apcTestParam)]} {
             PCLRuntimeData::MaterialContext::SetGenericSiViewLotScriptParameter $aRunContext(apcTestParam) "1"

        }


        #
        # reset run time; which is used by SiView limit check
        #
        if {$sResultCode != "SKIP"} {
            InteractionBaseline::StoreArrayInDS_lot "Layer StartTimeSkip LotNumber $sLotID" "nStartTime {}"
        }

        #
        # SiView return
        # check CreateJobResultsDictionarySiView to understand this
        #

        switch $sResultCode {
            NO_SELECTION {
                set errorMsg "No wafer selections for processLayer=$sProcessLayer processType=$sProcessType for this operationID=$sOperationID department=$sDepartment"
                set aSiViewSkipResults(sReturn_Fail) $errorMsg
                $::oAmdAppLog writeError "-- Sampling.Skip::ProcessMetrologyData: $errorMsg"
                PCLDataHandling::ThrowWarning "<<Error>>" "Skip" $errorMsg "Sampling"
                InteractionBaseline::AbortRun "SAMPLING_13" $errorMsg
            }
            CONFLICT_SELECTION {
                set errorMsg "Conflicting wafer selections for processLayer=$sProcessLayer processType=$sProcessType for this operationID=$sOperationID department=$sDepartment"
                set aSiViewSkipResults(sReturn_Fail) $errorMsg
                $::oAmdAppLog writeError "-- Sampling.Skip::ProcessMetrologyData: $errorMsg"
                PCLDataHandling::ThrowWarning "<<Error>>" "Skip" $errorMsg "Sampling"
                InteractionBaseline::AbortRun "SAMPLING_16" $errorMsg
            }
            FMFG_FAILURE {
                set errorMsg "FMFG spec execution problem"
                set aSiViewSkipResults(sReturn_Fail) $errorMsg
                $::oAmdAppLog writeError "-- Sampling.Skip::ProcessMetrologyData: $errorMsg"
                PCLDataHandling::ThrowWarning "<<Error>>" "Skip" $errorMsg "Sampling"
                InteractionBaseline::AbortRun "SAMPLING_23" $errorMsg
            }
            SKIP {
                set aSiViewSkipResults(sReturn) "$sSelectedWaferCount|SKIP"
            }
            STAY {
                set aSiViewSkipResults(sReturn) "$sSelectedWaferCount|STAY"
            }
            SKIPFAIL -
            default {
                set errorMsg "Undefined error $sResultCode"
                set aSiViewSkipResults(sReturn_Fail) $errorMsg
                $::oAmdAppLog writeError "-- Sampling.Skip::ProcessMetrologyData: $errorMsg"
                PCLDataHandling::ThrowWarning "<<Error>>" "Skip" $errorMsg "Sampling"
                InteractionBaseline::AbortRun "SAMPLING" $errorMsg
            }
        }

        # Reporting; this is done asynchronously
        if {($sResultCode == "SKIP")||($sResultCode == "STAY")} {

            SetSkipParameter "sResultCode" $sResultCode
            SetSkipParameter "lWaferSelection" $lWaferSelection
            SetSkipParameter "nSelectedWaferCount" $nSelectedWaferCount
            SetSkipParameter "bManualSelection" $bManualSelection
            SetSkipParameter "sSelection" $sSelection
            SetSkipParameter "sPSM" $sPSM

        }

        $::oAmdAppLog writeTrace ">>> Sampling.Skip::ProcessMetrologyData: Did define aSiViewSkipResults(sReturn) : $aSiViewSkipResults(sReturn)"

        return

    }
    #
    proc CheckPSM {} {

        set sResultCode ""
        
        set sLotID [PCLRuntimeData::MaterialContext::GetLotID]
        set nOperationNumber [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "operationNumber" $sLotID]
        set sFamilyLotID [PCLRuntimeData::MaterialContext::GetFamilyLotID $sLotID]

        array unset aPSMData
        if {[catch {SiViewHandling::GetExperimentalLotList $sFamilyLotID} sErr]} {
            $::oAmdAppLog writeError "-- SamplingSkip::CheckPSM: Can not obtain PSM for lot"
            set sResultCode "SKIPFAIL"
        } else {
            array set aPSMData $sErr
        }

        foreach nIndex $aPSMData(lPSMIndices) {
            if {($aPSMData($nIndex,splitOperationNumber) == $nOperationNumber)&&($aPSMData($nIndex,execFlag) != "1")} {
                $::oAmdAppLog writeWarning "-- SamplingSkip::CheckPSM: $aPSMData($nIndex,splitOperationNumber),$aPSMData($nIndex,mergeOperationNumber),$aPSMData($nIndex,routeID),$aPSMData($nIndex,testMemo)"
                set sResultCode "PSM"
                break
            }
        }

        if {$sResultCode == ""} {
            # assume that no PSM has been defined
            $::oAmdAppLog writeTrace "-- SamplingSkip::CheckPSM: $nOperationNumber has no PSMs for this lot"
        }

        return $sResultCode

    }
    #
    proc CheckListSampling { sLotID sOperationID sPL sPT bManualSelection sResultCode } {

        global aSiViewSkipResults

        # get config
        PCLRuntimeData::DOM::InitializeConfig
        set _Config_ [PCLRuntimeData::DOM::GetConfig]
        set _ConfigSchema_ [PCLRuntimeData::DOM::GetConfigSchema]

        set bLotHold 0
        set bEmail 0

        # is it a ListSampling lot anyway
        set sListSamplingID [PCLRuntimeData::MaterialContext::GetListSamplingLotScriptParameter $sLotID]
        if {$sListSamplingID == ""} {
            return
        }

        # check for setup type of list sampling
        set sListSamplingID [string trim $sListSamplingID]
        set sSpecPrefix [XMLHandling::XML.Get $_ConfigSchema_ $_Config_ "Configuration" "SpecPrefix"]
        set sRoutePrefix [XMLHandling::XML.Get $_ConfigSchema_ $_Config_ "Configuration" "RoutePrefix"]
        set bRefineRoutePrefix [XMLHandling::XML.Get $_ConfigSchema_ $_Config_ "Configuration" "RefineRoutePrefix"]
        set sRouteID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "routeID" $sLotID]
        if {[string range $sRouteID 0 2] != $sRoutePrefix} {
            if {$bRefineRoutePrefix == "true"} {
                set sRouteID [PCLRuntimeData::MaterialContext::GetEstimatedRouteID $sLotID]
            }
        }
        set sSpecID [string trim [lindex [split $sListSamplingID ","] 0]]
        set sProjectID [string trim [lindex [split $sListSamplingID ","] 1]]
        $::oAmdAppLog writeTrace "-- SamplingSkip::CheckListSampling: Lot has spec based sampling list. routeID=$sRouteID specID=$sSpecID projectID=$sProjectID"

        # what is setup in the list
        set sResultCodeList [PCLDataCollection::apcLotData::GetListSamplingValue $sSpecID $sRouteID $sProjectID $sOperationID]

        # list is not available
        if {$sResultCodeList == "NO_LIST_DEFINED"} {
            set bLotHold 1
            set bEmail 1
            set sErrorMsg "The sampling list $sListSamplingID for the lot $sLotID is not defined. Check the definition at the lot and/or provide the list"
            set sHoldText $sErrorMsg
            set sMailText $sErrorMsg
            $::oAmdAppLog writeError "-- SamplingSkip::CheckListSampling: $sErrorMsg"
        } elseif {$sResultCodeList == "NO_SPEC"} {
            set bLotHold 1
            set bEmail 1
            set sErrorMsg "The sampling specID $sSpecID for the lot $sLotID is not defined. Check the lot script parameter"
            set sHoldText $sErrorMsg
            set sMailText $sErrorMsg
            $::oAmdAppLog writeError "-- SamplingSkip::CheckListSampling: $sErrorMsg"
        } elseif {$sResultCodeList == "NO_REFERENCE"} {
            set bLotHold 1
            set bEmail 1
            set sErrorMsg "The sampling projectID $sProjectID and routeID $sRouteID has no valid list in spec ID $sSpecID for the lot $sLotID. Check the lot script parameter"
            set sHoldText $sErrorMsg
            set sMailText $sErrorMsg
            $::oAmdAppLog writeError "-- SamplingSkip::CheckListSampling: $sErrorMsg"
        } elseif {($sResultCodeList == "NO_PD")||($sResultCodeList == "NO_SUPER_ROCKET")} {
            set bLotHold 0
            set bEmail 0
            set sErrorMsg "The operationID $sOperationID is not setup in $sListSamplingID for the lot $sLotID. Check the spec"
            set sHoldText $sErrorMsg
            set sMailText $sErrorMsg
            $::oAmdAppLog writeError "-- SamplingSkip::CheckListSampling: $sErrorMsg"
            $::oAmdAppLog writeError "-- SamplingSkip::CheckListSampling: The operationID $sOperationID is not setup in the sampling list"
            # premature exit
            return
        } elseif {(($sResultCodeList == "SKIP")&&($sResultCode == "STAY"))||(($sResultCodeList == "STAY")&&($sResultCode == "SKIP"))} {
            set sErrorMsg "The sampling decision $sResultCode is different from $sResultCodeList given by the sampling list $sListSamplingID"
            # check for manual selection
            if {$bManualSelection} {
                $::oAmdAppLog writeWarning "-- SamplingSkip::CheckListSampling: $sErrorMsg. However, this is a manual selection"
                # premature exit
                return
            }
            # check for EMS
            set bEMS 0
            if {[catch {WaferSelectionHandling::WaferSelection.GetSamplingReason $sLotID $sPL $sPT} sErr]} {
                $::oAmdAppLog writeWarning "-- SamplingSkip::CheckListSampling: Can not get the sampling reason to check EMS"
            } else {
                array unset aSamplingReason
                array set aSamplingReason $sErr
                if {[info exists aSamplingReason(sOverwriteEMS)]&&($aSamplingReason(sOverwriteEMS) != "")} {
                    $::oAmdAppLog writeWarning "-- SamplingSkip::CheckListSampling: $sErrorMsg. However, this is an EMS selection"
                    # premature exit
                    return
                }
            }
            set bLotHold 1
            set bEmail 1
            set sHoldText $sErrorMsg
            set sMailText $sErrorMsg
            $::oAmdAppLog writeError "-- SamplingSkip::CheckListSampling: $sErrorMsg"
        }

        # mail
        if {$bEmail} {
            set lRecipient ""
            array unset aParameter 
            set aParameter(Type) "ListSampling"
            set _emailRecipients_ [XMLHandling::XML.Get $_ConfigSchema_ $_Config_ "Configuration" "EmailRecipientByTypeNodes" [array get aParameter]]
            foreach {_emailRecipient_} $_emailRecipients_ {
                lappend lRecipient [XMLHandling::XML.Get $_ConfigSchema_ $_emailRecipient_ "Recipient" "EMail"]
            }
            set lRecipient [string trim $lRecipient]
            PCLDataHandling::ThrowWarning "<<Error>>" "SamplingSkip::CheckListSampling" $sMailText "IQSampling" $lRecipient
        }

        # lot hold
        if {$bLotHold} {
            set aSiViewSkipResults(sReturn_Fail) "Error: SAMPLING_22: $sHoldText"
            InteractionBaseline::AbortRun "SAMPLING_22" $sHoldText
        }

        return

    }
    #
    proc CheckWaferSelection {} {

        array unset aResults

        set sLotID [PCLRuntimeData::MaterialContext::GetLotID]
        set lWaferID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "lWafer,waferID" $sLotID]
        set lSlotNumber [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "lWafer,slotNumber" $sLotID]
        set sProcessType [PCLRuntimeData::MaterialContext::GetGenericPDUData "sProcessTypeWfrSelection" $sLotID]
        set sProcessLayer [PCLRuntimeData::MaterialContext::GetGenericPDUData "sProcessLayer" $sLotID]

        array unset aWaferSelection
        array set aWaferSelection [WaferSelectionHandling::GetSelectionAMMO $lWaferID $sProcessType $sProcessLayer]  

        # number of selected wafer
        set nSelectedWaferCount 0
        set bManualSelection 0
        foreach sWaferScribe $lWaferID {
            if {$aWaferSelection($sWaferScribe,selectNAME) == "MUST"} {
                incr nSelectedWaferCount
                if {$aWaferSelection($sWaferScribe,selectingSystem) != "APCUSER"} {
                    $::oAmdAppLog writeWarning "-- SamplingSkip::CheckWaferSelection: Manual $aWaferSelection($sWaferScribe,selectNAME) selection for $sWaferScribe by $aWaferSelection($sWaferScribe,selectingSystem)"
                    set bManualSelection 1
                }
            }
            if {$aWaferSelection($sWaferScribe,selectNAME) == "EXCLUDE"} {
                if {$aWaferSelection($sWaferScribe,selectingSystem) != "APCUSER"} {
                    $::oAmdAppLog writeWarning "-- SamplingSkip::CheckWaferSelection: Manual $aWaferSelection($sWaferScribe,selectNAME) selection for $sWaferScribe by $aWaferSelection($sWaferScribe,selectingSystem)"
                    set bManualSelection 1
                }
            }
            lappend aResults(lWaferSelection) $aWaferSelection($sWaferScribe,selectNAME)
        }
        set aResults(nSelectedWaferCount) $nSelectedWaferCount

        set bAnyNODATA 0
        set bAnyCONFLICT 0
        set bAnyMUST 0
        foreach sWaferScribe $lWaferID {
            if {$aWaferSelection($sWaferScribe,selectNAME) == "NODATA"} {
                set bAnyNODATA 1
                break
            }
            if {$aWaferSelection($sWaferScribe,selectNAME) == "CONFLICT"} {
                set bAnyCONFLICT 1
                break
            }
            if {$aWaferSelection($sWaferScribe,selectNAME) == "MUST"} {
                set bAnyMUST 1
            }
        }

        # selection for AH
        set sSelection ""
        for {set nSlot 1} {$nSlot <= 25} {incr nSlot} {
            set bSelection "_"
            set nIndex [lsearch $lSlotNumber $nSlot]
            if {$nIndex != "-1"} {
                set bSelection "?"
                set sWaferScribe [lindex $lWaferID $nIndex]
                if {$sWaferScribe != ""} {
                    if {$aWaferSelection($sWaferScribe,selectNAME) == "MUST"} {
                        set bSelection "1"
                    }
                    if {$aWaferSelection($sWaferScribe,selectNAME) == "DO_NOT_CARE"} {
                        set bSelection "0"
                    }
                    if {$aWaferSelection($sWaferScribe,selectNAME) == "EXCLUDE"} {
                        set bSelection "-1"
                    }
                }
            }
            append sSelection $bSelection
        }
        set aResults(sSelection) $sSelection

        # for lotscript parameter
        if {$nSelectedWaferCount < 10} {
            set sSelectedWaferCount "WCNT=0$nSelectedWaferCount"
        } else {
            set sSelectedWaferCount "WCNT=$nSelectedWaferCount"
        }
        set aResults(sSelectedWaferCount) $sSelectedWaferCount

        if {$bAnyNODATA} {
            $::oAmdAppLog writeError "-- SamplingSkip::CheckWaferSelection: No valid wafer selection"
            set aResults(sResultCode) "NO_SELECTION"
            set aResults(bManualSelection) $bManualSelection
        } elseif {$bAnyCONFLICT} {
            $::oAmdAppLog writeTrace "-- SamplingSkip::CheckWaferSelection: Conflict in wafer selection"
            set aResults(sResultCode) "CONFLICT_SELECTION"
            set aResults(bManualSelection) $bManualSelection
        } elseif {$bAnyMUST} {
            $::oAmdAppLog writeTrace "-- SamplingSkip::CheckWaferSelection: Has selected wafers - stay"
            set aResults(sResultCode) "STAY"
            set aResults(bManualSelection) $bManualSelection
        } else {
            $::oAmdAppLog writeTrace "-- SamplingSkip::CheckWaferSelection: No selected wafers - skip"
            set aResults(sResultCode) "SKIP"
            set aResults(bManualSelection) $bManualSelection
        }

        return [array get aResults]

    }
    #
    # @desc support for emergency skip
    #
    proc EmergencySkip { sRouteID sOperationID sPL sPT sSubLotType sRate sOverwrite sOverwriteEMS sOverwriteLIST } {

        set bCandidate 0

        # check for overwrite
        if {($sOverwrite != "")||($sOverwriteEMS != "")||($sOverwriteLIST != "")} {
            $::oAmdAppLog writeTrace "-- SamplingSkip::EmergencySkip: No skip candidate since at least one overwrite is active sOverwrite $sOverwrite sOverwriteEMS $sOverwriteEMS sOverwriteLIST $sOverwriteLIST"
            return $bCandidate
        }

        # check for rate X -> RateX%
        set nRate [string range $sRate 4 [expr [string length $sRate] - 2]]
        if {[catch {expr $nRate*1.0}]} {
            $::oAmdAppLog writeTrace "-- SamplingSkip::EmergencySkip: No skip candidate since nRate $nRate is not numeric"
            return $bCandidate
        }
        if {$nRate > 90} {
            $::oAmdAppLog writeTrace "-- SamplingSkip::EmergencySkip: No skip candidate since nRate $nRate is >90"
            return $bCandidate
        }
        if {$nRate < 15} {
            $::oAmdAppLog writeTrace "-- SamplingSkip::EmergencySkip: No skip candidate since nRate $nRate is <15"
            return $bCandidate
        }

        # is the PD in spec to be skipped
        PCLRuntimeData::DOM::InitializeConfig
        set _Config_ [PCLRuntimeData::DOM::GetConfig]
        set _ConfigSchema_ [PCLRuntimeData::DOM::GetConfigSchema]
        array unset aParameter
        set aParameter(Type) "ListSampling"
        set sSpecID [XMLHandling::XML.Get $_ConfigSchema_ $_Config_ "Configuration" "SpecSpecialSpecIDByType" [array get aParameter]]
        set sResultCodeList [PCLDataCollection::apcLotData::GetListSamplingValue $sSpecID "EXCEPTION" "EMERGENCYSKIP" $sOperationID]
        if {$sResultCodeList == "SKIP"} {
            $::oAmdAppLog writeTrace "-- SamplingSkip::EmergencySkip: Skip candidate since setup in spec and no pre condition checked violated sRate $sRate sOverwrite $sOverwrite sOverwriteEMS $sOverwriteEMS sOverwriteLIST $sOverwriteLIST"
            set bCandidate 1
        }

        return $bCandidate

    }
    #
    proc GetSkipParameter { sSkipParameterName } {

        variable ::SamplingSkip::aSkipParameter

        if {[info exists aSkipParameter($sSkipParameterName)]} {
            return $aSkipParameter($sSkipParameterName)
        } else {
            set errorMsg "Requested skip parameter -$sSkipParameterName- not set"
            $::oAmdAppLog writeError "-- SamplingSkip::GetSkipParameter: $errorMsg"
            return ""
        }

    }
    #
    proc SetSkipParameter { sSkipParameterName sSkipParameterValue } {

        variable ::SamplingSkip::aSkipParameter

        if {$sSkipParameterName != ""} {
            set aSkipParameter($sSkipParameterName) $sSkipParameterValue
        }

        return

    }
    #
    proc Report {} {

        global aRunContext aSiViewSkipResults

        variable aHistoryEntriesSkip
        variable aHistoryEntriesStay
        variable aHistoryEntriesReason

        #
        # context to be stored
        #

        set nExitTimeStamp [UtilityBaseline::MakeTimeStamp]

        set sResultCode [GetSkipParameter "sResultCode"]
        set lWaferSelection [GetSkipParameter "lWaferSelection"]
        set nSelectedWaferCount [GetSkipParameter "nSelectedWaferCount"]
        set bManualSelection [GetSkipParameter "bManualSelection"]
        set sSelection [GetSkipParameter "sSelection"]
        set sPSM [GetSkipParameter "sPSM"]

        # statistic initialization
        set sLotID [PCLRuntimeData::MaterialContext::GetLotID]
        set nIndex [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "nIndex" $sLotID]

        set sStartTime $aRunContext(SiViewTimeStamp)
        set nStartTime [PCLDataHandling::ConvertSiViewTimeStamp $sStartTime]
        set nSpecCount [PCLRuntimeData::GetSpecCount]
        if {$nSpecCount == ""} {
            set nSpecCount 0
        }
        set nSkipSpecCount $nSpecCount
        set nPostSpecCount 0
        set sPostOperationID ""
        set nPostSpecRunTime 0


        # check the run time
        set nSkipRunTime [MathBaseline::SafeExpr "[clock scan [UtilityBaseline::ConvertTimeStamp $nExitTimeStamp]] - [clock scan [UtilityBaseline::ConvertTimeStamp $nStartTime]]"]

        # check the offset
        set nScriptStartTime [PCLRuntimeData::GetStartTime]
        catch { set nScriptStartTime $::_aPerformanceData(ScriptStartTimeStamp) }
        set nOffsetRunTime [MathBaseline::SafeExpr "[clock scan [UtilityBaseline::ConvertTimeStamp $nScriptStartTime]] - [clock scan [UtilityBaseline::ConvertTimeStamp $nStartTime]]"]

        $::oAmdAppLog writeTrace "-- SamplingSkip::Report: The times nExitTimeStamp $nExitTimeStamp nStartTime $nStartTime nScriptStartTime $nScriptStartTime nSkipRunTime $nSkipRunTime nOffsetRunTime $nOffsetRunTime"

        # get the statistic
        set lDBKey ""
        lappend lDBKey "LotID" $sLotID
        lappend lDBKey "Type" "SkipStatistic"
        lappend lDBKey "PL" "ALL"
        lappend lDBKey "PT" "ALL"
        if {[catch {DatabaseInterface::RetrieveGenericRecord "PCLMHIST" $lDBKey} sErr]} {
            $::oAmdAppLog writeError "-- Sampling.Skip::CheckSiViewLimits: $sErr"
        } else {
            array set aPre1Statistic $sErr
        }
        if {$nIndex == 0} {
            DatabaseInterface::StoreGenericRecord "PCLMHIST" $lDBKey "nStartTime $nStartTime nSkipSpecCount $nSkipSpecCount"
            if {[info exists aPre1Statistic(sPostOperationID)]&&[info exists aRunContext(completedProcessID)]&&($aPre1Statistic(sPostOperationID) == $aRunContext(completedProcessID))} {
                set nPostSpecCount $aPre1Statistic(nPostSpecCount)
                set sPostOperationID $aPre1Statistic(sPostOperationID)
                set nPostSpecRunTime $aPre1Statistic(nPostSpecRunTime)
            }
        } else {
            if {[info exists aPre1Statistic(nSkipSpecCount)]} {
                set nStartTime $aPre1Statistic(nStartTime)
                set nSkipSpecCount [expr $aPre1Statistic(nSkipSpecCount) + $nSkipSpecCount]
                DatabaseInterface::StoreGenericRecord "PCLMHIST" $lDBKey "nSkipSpecCount $nSkipSpecCount"
            } else {
                DatabaseInterface::StoreGenericRecord "PCLMHIST" $lDBKey "nStartTime $nStartTime nSkipSpecCount $nSkipSpecCount"
            }
        }

        set sOperationID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "operationID" $sLotID]
        set nOperationNumber [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "operationNumber" $sLotID]
        set sRouteID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "routeID" $sLotID]
        set lWaferID [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "lWafer,waferID" $sLotID]
        set sMaskLevel [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "maskLevel" $sLotID]
        set sPL [PCLRuntimeData::MaterialContext::GetGenericPDUData "sProcessLayer" $sLotID]
        set sPT [PCLRuntimeData::MaterialContext::GetGenericPDUData "sProcessTypeWfrSelection" $sLotID]
        set bMandatoryOperationFlag [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "mandatoryOperationFlag" $sLotID]
        set nIndex [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "nIndex" $sLotID]
        set sSubLotType [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "subLotType" $sLotID]
        set sDepartment [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "department" $sLotID]
        set nDay [lindex [split $nExitTimeStamp "."] 0]

        # special material
        set sSkipFastProd [PCLRuntimeData::MaterialContext::GetFastProdLotScriptParameter $sLotID]
        set sSkipSuperRocket [PCLRuntimeData::MaterialContext::GetListSamplingLotScriptParameter $sLotID]

        # the pass count
        set nOperationPassCount [PCLRuntimeData::MaterialContext::GetGenericSiViewLotData "operationPassCount" $sLotID]
        if {$nOperationPassCount == ""} {
            set nOperationPassCount "1"
        }

        # the sampling reason
        if {[catch {WaferSelectionHandling::WaferSelection.GetSamplingReason $sLotID $sPL $sPT} sErr]} {
            $::oAmdAppLog writeWarning  "-- SamplingSkip::Report: Can not get the sampling reason"
        } else {
            array unset aSamplingReason
            array set aSamplingReason $sErr
            if {$bManualSelection == "1"} {
                if {[info exists aSamplingReason(sOverwrite)]} {
                    set aSamplingReason(sOverwrite) "$aSamplingReason(sOverwrite) ManualSelection"
                    set aSamplingReason(sOverwrite) [string trim $aSamplingReason(sOverwrite)]
                } else {
                    set aSamplingReason(sOverwrite) "ManualSelection" 
                }
            }
            # for MCM
            set lCheckedReason "sOriginator sOrigin sTableName sContextName sRate sOverwrite sOverwriteEMS sOverwriteLIST nTimeStamp"
            foreach {sReason} $lCheckedReason {
                set sReasonAH "Reason[string range $sReason 1 [expr [string length $sReason] - 1]]"
                if {[info exists aSamplingReason($sReason)]} {
                    set aHistoryEntriesStay($sReasonAH) $aSamplingReason($sReason)
                } else {
                    set aHistoryEntriesStay($sReasonAH) ""
                }
            }
            if {[info exists aSamplingReason(bRate)]} {
                set aHistoryEntriesStay(ReasonRateBool) $aSamplingReason(bRate)
            } else {
                set aHistoryEntriesStay(ReasonRateBool) ""
            }

            # for history in general
            if {[info exists aSamplingReason(sOriginator)]} {
                set aHistoryEntriesReason(sSpecID) $aSamplingReason(sOriginator)
            }
            if {[info exists aSamplingReason(sOrigin)]} {
                set aHistoryEntriesReason(sDecisionOperationID) $aSamplingReason(sOrigin)
            }
            if {[info exists aSamplingReason(sOriginEQP)]} {
                set aHistoryEntriesReason(sDecisionEquipmentID) $aSamplingReason(sOriginEQP)
            }
            if {[info exists aSamplingReason(nTimeStamp)]} {
                set aHistoryEntriesReason(sDecisionTimeStamp) $aSamplingReason(nTimeStamp)
            }
            if {[info exists aSamplingReason(sTableName)]} {
                set aHistoryEntriesReason(sSpecTableName) $aSamplingReason(sTableName)
            }
            if {[info exists aSamplingReason(sContextName)]} {
                set aHistoryEntriesReason(sSpecContextName) $aSamplingReason(sContextName)
            }
            if {[info exists aSamplingReason(sApplication)]} {
                set aHistoryEntriesReason(sApplication) $aSamplingReason(sApplication)
            }
            if {[info exists aSamplingReason(bRate)]} {
                set aHistoryEntriesReason(bRate) $aSamplingReason(bRate)
            }
            if {[info exists aSamplingReason(sRate)]} {
                set aHistoryEntriesReason(sRate) $aSamplingReason(sRate)
            }
            if {[info exists aSamplingReason(sOverwrite)]} {
                set aHistoryEntriesReason(sOverwrite) $aSamplingReason(sOverwrite)
            }
            if {[info exists aSamplingReason(sOverwriteEMS)]} {
                set aHistoryEntriesReason(sOverwriteEMS) $aSamplingReason(sOverwriteEMS)
            }
            if {[info exists aSamplingReason(sOverwriteLIST)]} {
                set aHistoryEntriesReason(sOverwriteLIST) $aSamplingReason(sOverwriteLIST)
            }
            if {[info exists aSamplingReason(sOverwriteNOTE)]} {
                set aHistoryEntriesReason(sOverwriteNOTE) $aSamplingReason(sOverwriteNOTE)
            }
        }

        if {$sResultCode == "STAY"} {
            set bSampling 1
        } else {
            set bSampling 0
        }

        #
        # AH_SKIP --> needed for migration
        #

        set aHistoryEntriesSkip(lCheckedOperationIDs) $sOperationID
        set aHistoryEntriesSkip(lCheckedOperationNumbers) $nOperationNumber
        set aHistoryEntriesSkip(lWaferScribes) $lWaferID
        set aHistoryEntriesSkip(lProcessLayer) $sPL
        set aHistoryEntriesSkip(lProcessTypeWfrSelection) $sPT
        set aHistoryEntriesSkip(lMandatory) $bMandatoryOperationFlag
        set aHistoryEntriesSkip(sSubLotType) $sSubLotType
        set aHistoryEntriesSkip(SkipFastProd) $sSkipFastProd
        set aHistoryEntriesSkip(SkipSuperRocket) $sSkipSuperRocket
        set aHistoryEntriesSkip(sSiViewReturn) $sResultCode
        set aHistoryEntriesSkip(lWaferSelection) $lWaferSelection
        set aHistoryEntriesSkip(nSiViewWaitTime) $nSkipRunTime
        set aHistoryEntriesSkip(lPSMs) $sPSM

        set lHistoryParamListSkip ""
        foreach {sName} [array names aHistoryEntriesSkip] {
            lappend lHistoryParamListSkip [list SamplingSkip::aHistoryEntriesSkip($sName) combine $sName long_string]
        }
        if {[catch {InteractionBaseline::RecordDataInApplicationHistory "SKIP" $lHistoryParamListSkip} sErr]} {
            $::oAmdAppLog writeWarning "-- SamplingSkip::Report: Problem storing AH_SKIP: $sErr"
        }

        #
        # AH_SKIP_STAYREASON
        #

        if {$sResultCode == "STAY"} {

            set aHistoryEntriesStay(StayOperationID) $sOperationID
            set aHistoryEntriesStay(StayOperationNumber) $nOperationNumber
            set aHistoryEntriesStay(StayRouteID) $sRouteID
            set aHistoryEntriesStay(StayPL) $sPL
            set aHistoryEntriesStay(StayPT) $sPT
            set aHistoryEntriesStay(sSubLotType) $sSubLotType
            set aHistoryEntriesStay(StayOperationPassCount) $nOperationPassCount
            # sampling reason already set in init

            # support for emergency skip
            set bCandidate [EmergencySkip $sRouteID $sOperationID $sPL $sPT $sSubLotType $aHistoryEntriesStay(ReasonRate) $aHistoryEntriesStay(ReasonOverwrite) $aHistoryEntriesStay(ReasonOverwriteEMS) $aHistoryEntriesStay(ReasonOverwriteLIST)]
            set aHistoryEntriesStay(SkipCandidate) $bCandidate

            set lHistoryParamListStay ""
            foreach {sName} [array names aHistoryEntriesStay] {
                lappend lHistoryParamListStay [list SamplingSkip::aHistoryEntriesStay($sName) combine $sName long_string]
            }
            if {[catch {InteractionBaseline::RecordDataInApplicationHistory "SKIP_STAYREASON" $lHistoryParamListStay} sErr]} {
                $::oAmdAppLog writeWarning "-- SamplingSkip::Report: Problem storing AH_SKIP_STAYREASON: $sErr"
            }

        }

        #
        # AH_SKIP_REASON
        #

        set aHistoryEntriesReason(sOperationID) $sOperationID
        set aHistoryEntriesReason(nOperationNumber) $nOperationNumber
        set aHistoryEntriesReason(nOperationPassCount) $nOperationPassCount
        set aHistoryEntriesReason(nOperationSequence) $nIndex
        set aHistoryEntriesReason(nSkipSpecCount) $nSkipSpecCount
        set aHistoryEntriesReason(nPostSpecCount) $nPostSpecCount
        set aHistoryEntriesReason(nSpecCount) $nSpecCount
        set aHistoryEntriesReason(nExitTimeStamp) $nExitTimeStamp
        set aHistoryEntriesReason(nPostSpecRunTime) $nPostSpecRunTime
        set aHistoryEntriesReason(nSiViewWaitTime) $nSkipRunTime
        set aHistoryEntriesReason(nOffsetTime) $nOffsetRunTime
        set aHistoryEntriesReason(sPostOperationID) $sPostOperationID
        set aHistoryEntriesReason(sRouteID) $sRouteID
        set aHistoryEntriesReason(sML) $sMaskLevel
        set aHistoryEntriesReason(sPL) $sPL
        set aHistoryEntriesReason(sPT) $sPT
        set aHistoryEntriesReason(sSubLotType) $sSubLotType
        set aHistoryEntriesReason(sDepartment) $sDepartment
        set aHistoryEntriesReason(lWaferScribes) $lWaferID
        set aHistoryEntriesReason(bMandatory) $bMandatoryOperationFlag
        set aHistoryEntriesReason(sListSampling) $sSkipSuperRocket
        set aHistoryEntriesReason(sSiViewReturn) $sResultCode
        set aHistoryEntriesReason(sPSM) $sPSM
        set aHistoryEntriesReason(nDay) $nDay
        set aHistoryEntriesReason(nSelectedWaferCount) $nSelectedWaferCount
        set aHistoryEntriesReason(sSelection) $sSelection
        set aHistoryEntriesReason(bSampling) $bSampling

        # sampling reason already set in init

        set lHistoryParamListReason ""
        foreach {sName} [array names aHistoryEntriesReason] {
            lappend lHistoryParamListReason [list SamplingSkip::aHistoryEntriesReason($sName) combine $sName long_string]
        }
        if {[catch {InteractionBaseline::RecordDataInApplicationHistory "SKIP_REASON" $lHistoryParamListReason} sErr]} {
            $::oAmdAppLog writeWarning "-- SamplingSkip::Report: Problem storing AH_SKIP_REASON: $sErr"
        }

        # LotManager reporting
        if {($bMandatoryOperationFlag == "0")&&($sResultCode == "STAY")} {
            LotManager::UpdateSampling $sLotID "operationID $sOperationID" "nSelectedWafer $nSelectedWaferCount"
        }

        $::oAmdAppLog writeTrace "-- SamplingSkip::Report"

        return

    }

}