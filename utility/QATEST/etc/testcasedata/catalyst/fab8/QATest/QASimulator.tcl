# (c) Copyright 2011 GLOBALFOUNDRIES Inc. All rights reserved.
#       GLOBALFOUNDRIES, the GLOBALFOUNDRIES logo, CATALYST, and the
#       combinations thereof are trademarks of GLOBALFOUNDRIES Inc.

lappend ::lControllers QASimulator

namespace eval QASimulator {

    variable returnValues

    proc DefineControlParams { } {
        $::oAmdAppLog writeTrace "DefineControlParams  ..."
        array set ::QASimulator::returnValues [list]
        return
    }

    proc SetupProcessEventHandlers {} {
        $::oAmdAppLog writeError "SetupProcessEventHandlers ..."
        doTests
        return
    }
    
    # Simplified tuning confirmation
    proc ConfirmSettings {sLot sTool sParamName} {

        set bRepeat 1

        set sGuiMessage     "Overwrite APC Tunings ($sParamName)\n"
        append sGuiMessage  "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
        append sGuiMessage  "\n"
        append sGuiMessage  "Equipment      $sTool\n"
        append sGuiMessage  "Lot            $sLot\n"
        set lGuiButton ""
        lappend lGuiButton [list "Spacer1"  {Class "LineSeparator"}]        
        lappend lGuiButton [list "Spacer2"  {Class "LineSeparator"}]
        lappend lGuiButton [list "sReason" [list Class "TextField" Label "Reason" Default "" Color "black"]]

        set lButtons [list [list "<OK>" true] [list "<CANCEL>" false]]

        while {$bRepeat} {
            catch {array unset aGuiResults}
            if {[catch {APCBaseline::FabGUI::ViewExtendedQuery $sTool $sGuiMessage $lGuiButton $lButtons 300000} cerrout]} {
                $::oAmdAppLog writeError "Error in ViewExtendedQuery in ActionPerformReset: $cerrout"
                InteractionBaseline::AbortRun "COMMFAIL" "Error in ViewExtendedQuery: $cerrout" 
                return
            } else {
                array set aGuiResults [APCBaseline::FabGUI::ParseExtendedQueryResults [lindex $cerrout 1]]
            }
            $::oAmdAppLog writeTrace [UtilityBaseline::PrintArray "aGuiResults"]
            switch -- [lindex $cerrout 0] {
               "<OK>" {
                   set bRepeat 0                
                }
                "<CANCEL>" {
                    InteractionBaseline::AbortRun "USERABORT"
                    return 
                }
                default {
                    set UtilityBaseline::aApcErrorMessage(COMMFAIL) "APC waits for FabGui-Answer from user. Maybe Timeout or DataTransferProblem"
                    InteractionBaseline::AbortRun "COMMFAIL" "Default-Error in ViewExtendedQuery: $cerrout" 
                    return
                }
            }
        }
        set lResults "OK"
        return $lResults
    }

    proc doTests { } {
        variable testResult 1
        variable returnValues
        set oStringDictionary [$::controlJobAdmin getParameters]
        $oStringDictionary asArray jobData
        $::oAmdAppLog writeTrace [UtilityBaseline::PrintArray jobData]
        set context [$::controlJobAdmin getContext]
        ##set lot [$context getValue "lotID"]
        #
        # command loop like execution, sorted by step.i.<testCase>
        foreach k [lsort [array names jobData -glob "step\.*\.*" ]] {
            $::oAmdAppLog writeTrace ">>> begin testcase $k  $jobData($k)"
            set testCaseResult "OK"
            set testCase [lindex [split $k .] 2]
	          set queue "fab8stg"
            switch $testCase {
                LongSleep { after $jobData($k) }
                RestartWorkerFlag { java::call com.amd.catalyst.wpm.WorkerProcess {restartAfterCurrentJob} }
                SetWaferSelection {
                    set wafers [list]
                    foreach aw [array names jobData -glob "SetWaferSelection.wafer*" ] {lappend wafers $jobData($aw)}
                    set operationType $jobData(SetWaferSelection.operationType)
                    set photoLayer $jobData(SetWaferSelection.photoLayer)
                    if { [catch { 
                        ::APCBaseline::Ammo::SetWaferSelection $wafers $operationType $photoLayer "MUST"
                    } res]} {
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                GetWaferSelection {
                    set wafers [list]
                    foreach aw [array names jobData -glob "GetWaferSelection.wafer*" ] {lappend wafers $jobData($aw)}
                    set operationType $jobData(GetWaferSelection.operationType)
                    set photoLayer $jobData(GetWaferSelection.photoLayer)
                    if { [catch { 
                        ::APCBaseline::Ammo::GetSelectedWafers $wafers $operationType $photoLayer 
                    } res]} {
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                RemoveWaferSelection {
                    set wafers [list]
                    foreach aw [array names jobData -glob "RemoveWaferSelection.wafer*" ] {lappend wafers $jobData($aw)}
                    set operationType $jobData(RemoveWaferSelection.operationType)
                    set photoLayer $jobData(RemoveWaferSelection.photoLayer)
                    if { [catch { 
                        ::APCBaseline::Ammo::RemoveWaferSelection $wafers $operationType $photoLayer 
                    } res]} {
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                SETUPFC {
                    if { [catch {
                        APCBaseline::Pcms::FindCurrentVersion $jobData($k)
                    } res]} { 
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                LotInfo {
                    if { [catch {
                        ::APCBaseline::SiViewTxMethods::TxLotInfoInq $jobData($k) ""
                    } res]} { 
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                FutureHold {
                    if { [catch {
                        APCBaseline::SiViewTxMethods::TxEnhancedFutureHoldReq $jobData($k) "FutureHold" $jobData(FutureHold.Route) $jobData(FutureHold.OpNo) $jobData(FutureHold.ReasonCode) "" "1" "1" "CatalystTest"
                        APCBaseline::SiViewTxMethods::TxFutureHoldCancelReq $jobData($k) $jobData(FutureHold.ReleaseReasonCode) "Cancel" "FutureHold" $jobData(FutureHold.ReasonCode) "X-CATALYST" "C" $jobData(FutureHold.Route) $jobData(FutureHold.OpNo) "CatalystTest"
                    } res]} { 
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                EqpInfo {
                    if { [catch {
                        ::APCBaseline::SiViewTxMethods::TxEqpInfoInq $jobData($k)
                    } res]} { 
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                BaselineAddOn {
                    if { [catch {
                        $::oAmdAppLog writeTrace "sleeping $jobData($k)ms"
                        after $jobData($k)
                        $::oAmdAppLog writeTrace "loading baseline addon"
                        UtilityBaseline::LoadBaselineAddOn "/scripts/PCL/PCLAddOn.plan"
                    } res]} { 
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }
                MQSend {
                    set msg { <?xml version="1.0" encoding="UTF-8"?>
                    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <soapenv:Body xmlns:ns1="http://www.amd.com/schemas/baselineservices">
                    <ns1:EIInformation/>
                    </soapenv:Body>
                    </soapenv:Envelope>
                    }
                    if { [catch {
                         set mqSender [catalyst::base::locator::locate "mq.MqSender"]
                         set mqParams [java::call [catjava::qualify "mq.MqParamsBuilder"] build]
                         $mqParams setJmsCompliant "true"
                         if { [info exists jobData(MQSend.Reply)]
                         } {
                           $::oAmdAppLog writeTrace $msg
                           set reply [$mqSender sendWithReply $queue $jobData($k) $msg $mqParams]
                           set returnValues(value.MQSend) $reply
                           $::oAmdAppLog writeTrace $reply
                         } else {
                           $mqSender sendNoReply $queue $jobData($k) $msg $mqParams
                         }
                    } res]} { 
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }
                }                
                FabGui {
                    if { [catch { 
                         ConfirmSettings "Lot" $jobData($k) "Parameter"
                    } res]} {
                        $::oAmdAppLog writeError $res
                        set testCaseResult $res
                    }                   
                }
                default { set testCaseResult "Error: testcase not defined in QASimulator" }
            }
            $::oAmdAppLog writeTrace "<<< end testcase $k  $jobData($k)"
            set returnValues($k) $testCaseResult
            set testResult [expr $testResult && {$testCaseResult == "OK"}]
        }
        #
        set returnValues(testResult) $testResult 
        #
        #return [array get aResults]    
    }
}

# this is a dirty hack to pass the raw results as key value list back to the test driver
rename FabSpecific::CreateJobResultsDictionary FabSpecific::CreateJobResultsDictionary_orig

proc FabSpecific::CreateJobResultsDictionary {oResults Status Code Description Number} {
    $::oAmdAppLog writeTrace "FabSpecific::CreateJobResultsDictionary QASimulator override"
    set oCjaResultsDict [FabSpecific::CreateJobResultsDictionary_orig $oResults $Status $Code $Description $Number]
    foreach k [array names ::QASimulator::returnValues] {
        ${oCjaResultsDict} setValue $k $::QASimulator::returnValues(${k})
    }
    return $oCjaResultsDict
}

## use default endscript
##proc EndScript {x level} {
##    $::oAmdAppLog writeTrace "QASimulator EndScript override - exited $level"
##    set ::job::preserve_shell "false"
##    $::oAmdAppLog writeTrace "deleting ITCL objects ..."
##    foreach o [find objects] { delete object $o }
##    $::oAmdAppLog writeTrace "deleting ITCL classes ..."
##    foreach o [find classes] { if ![string match "::Catalyst*" $o] { delete object $o } }
##    return
##}
