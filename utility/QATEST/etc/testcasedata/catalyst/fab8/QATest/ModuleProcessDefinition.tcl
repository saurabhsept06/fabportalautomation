# test TxModuleProcessDefinitionIDListInq

namespace eval APCBaseline::SiViewTxMethods {

proc TxModuleProcessDefinitionIDListInq {pdID pdType pdLevel version} {
        variable aErrorCodes
        variable sServiceName
        variable pDefaultErrorHandler
        variable sDefaultApcErrorCode

        set sMethodName "TxModuleProcessDefinitionIDListInq"

        set oParamDict [Catalyst::Dictionary::new]
        $oParamDict setValue pdID $pdID
        $oParamDict setValue pdType     $pdType
        $oParamDict setValue pdLevel $pdLevel
        $oParamDict setValue version $version


        set bCallExternalServicesFailed [catch {SendExternalService $sServiceName $sMethodName $oParamDict $pDefaultErrorHandler $sDefaultApcErrorCode} oResults]
        set savedErrorInfo  $::errorInfo
        set savedErrorCode  $::errorCode

        $oParamDict deleteContents
        Catalyst::Object::delete $oParamDict

        if {$bCallExternalServicesFailed} {
            return -code error -errorinfo $savedErrorInfo -errorcode $savedErrorCode $oResults
        } else {
            return $oResults
        }
    }

}

namespace eval APCBaseline::SiView {

namespace import -force ::APCBaseline::SiViewTxMethods::*

proc GetModuleProcessDefinition {pdID pdType pdLevel version} {
        if {[catch {TxModuleProcessDefinitionIDListInq $pdID $pdType $pdLevel $version} oResultDict]} {
            return -code error -errorinfo $::errorInfo -errorcode $::errorCode $oResultDict
        }

        set lResult [UtilityBaseline::StringifyDictionary $oResultDict]
        PropagateObjectIdentifier lResult

        $oResultDict deleteContents
        Catalyst::Object::delete $oResultDict

        return $lResult
    }

}