# $Id: BaselineMultiProcess.tcl 137679 2011-04-26 11:08:15Z tseiler $
# (C) Copyright Advanced Micro Devices, Inc., 1999-2008  All Rights Reserved
# (C) Copyright Global Foundries, Inc., 2009-2010 All Rights Reserved
# AMD proprietary / AMD Confidential

puts "Start Run"

set sScriptName "APCBaseline::MainScript::UtilityMainscript (BaselineUtilityMainscript.tcl)"
set nScriptVersion [lindex {$Revision: 137679 $} 1]
set aScriptVersions($sScriptName) $nScriptVersion

# some Baseline features are sensitive to sApplicationType
set sApplicationType "Utility"

$::oAmdAppLog writeTrace "Parsing $sScriptName - Version $nScriptVersion"
#*************************************************************************
$::oAmdAppLog setPrefix2 "BMU - "
#
#
set bUpdateJeopardy 1
set bSendApcDone 1
set nTimeStamp [UtilityBaseline::MakeTimeStamp]

# initialize performance log
set ::_aPerformanceData(ScriptStartTime) [clock seconds]
set ::_aPerformanceData(ScriptStartTimeStamp) [clock format $_aPerformanceData(ScriptStartTime) -format "%Y%m%d.%H%M%S"]
set ::UtilityBaseline::nStartTime [clock seconds]

############################################################################################
# tool-wide globals
# must set up lControllers
# this is a list of namespaces in which the different controllers reside
# this is an artefact and should be removed later
if {![info exists lControllers]} {
    set lControllers [list]
}

namespace eval APCBaseline::MainScript::UtilityMainscript {

    # import general purpose helper functions
    namespace import -force ::APCBaseline::MainScript::*

    $::oAmdAppLog setPrefix2 "BMU - "

    #######################################################################
    # @desc Run a process control job
    #
    #
    #
    proc run {lControllers} {
        # list of active controller namespaces

        #######################################################################
        #
        # Initialize Baseline
        #

        set sMainscriptExecutionStatus "UtilityStarted"
        # set Prefix to be used by generic MainScript procedures
        SetMainScriptPrefix "BMU"

        InitializeBaseline
        InitializeOperatorInterface $::InteractionBaseline::bUseGuis

        set lContext [array get ::aRunContext]

        # call initialize
        foreach sController $lControllers {
            $::oAmdAppLog writeTrace "Initialize controller $sController"

            CallFuncIfExists ${sController}::initialize $lContext
        }

        # call run
        foreach sController $lControllers {
            CallFuncIfExists ${sController}::run
        }

        # call cleanup
        foreach sController $lControllers {
            CallFuncIfExists ${sController}::cleanup
        }


        if {$::bSendApcDone} {
            $::oAmdAppLog writeTrace "This EI expects apcdone message -- sending now"
            catch {InteractionBaseline::EndApcRun}
        } else {
            $::oAmdAppLog writeTrace "This EI does not expect apcdone message -- skipping"
        }
    }
}

APCBaseline::MainScript::UtilityMainscript::run $lControllers

$::oAmdAppLog writeTrace "$::sScriptName finished - CleanUpAndExit"
UtilityBaseline::CleanUpAndExit
##################################################
# Script terminates here
##################################################
