# $Id: Fab36Config.tcl 100629 2013-02-05 13:51:59Z tseiler $
# (c) Copyright Advanced Micro Devices, Inc., 1999-2009  All Rights Reserved
# AMD proprietary / AMD Confidential
#
# Fab specific configuration, e.q. like email lists etc
#
##################################################################################
##################################################################################
##
##                   D E V E L O P M E N T   O N L Y
##
##                 DO NOT USE THIS FILE IN PRODUCTION
##
##                          ENVIRONMENT: AppSimulator
##################################################################################
##################################################################################

set aScriptVersions(Fab36Config.tcl) [lindex {$Revision: 100629 $} 1]


#As of Catalyst 3.2.3, OS_CATALYST_ROOT is no longer set
if {[info exists env(OS_CATALYST_ROOT)] } {
    set sCatalystRoot [file join $env(OS_CATALYST_ROOT)]
} else {
    set sCatalystRoot [file join $env(CATALYST_ROOT)]
}
if {![info exists lBaselineConfiguration]} {
    set lBaselineConfiguration [list]
}

set bWin64 "0"
if {[info exists env(PROCESSOR_ARCHITEW6432)] && $env(PROCESSOR_ARCHITEW6432) == "AMD64"} {
    set bWin64 "1"
}

# plan name must not be NA due to error handling in Baseline with unknown plan NA
set sCatalystPlanName "UNKNOWN"
if {![catch {$::controlJobAdmin getPlan} oPlan]} {
    if {[catch {$oPlan getName} sCatalystPlanName]} {
        puts "ERROR: Could not retrieve plan name: $sCatalystPlanName"
        set sCatalystPlanName "UNKNOWN"
    }
} else {
    puts "ERROR: Could not retrieve plan object from controlJobAdmin: $oPlan"
}

# DEBUG ONLY
catch {
    if {1 || $sCatalystPlanName == "UNKNOWN"} {
        set oReq [catjava::autocast $::job::request]
        puts "request='[$oReq toString]'"
    }
}

#
# set script execution environment to handle facility specific execution of scripts
#
# preset environment variables are:
#   FABENV=PROD|ITDC
#   FABNAME=FAB36
#
namespace eval UtilityBaseline {
    set sScriptExecutionEnvironment "FAB36"
    set sServerEnvironment "development"
}

# Define FDC Namesspace and variables potentially modified by Baseline config
#
namespace eval FDC {
    # @desc string AH table name to store FDC results to using the FDCRes Manager
    set sDefaultAHTableNameForResults "fdcres"
}

# set precision for double calculations to 12 to be compatible with TCL8.4
set ::tcl_precision 12

# These are here until we do a more thorough cleaning of the baseline to remove aRunContext refs and replace
# with the global config info
lappend lBaselineConfiguration [list aRunContext(lotID)    			   aRunContext lotID [list "usevalue NA"]]
lappend lBaselineConfiguration [list aRunContext(lotNumber)            aRunContext lotID         ignore]
lappend lBaselineConfiguration [list aRunContext(operation)            aRunContext operationID   ignore]
lappend lBaselineConfiguration [list aRunContext(ProcessDefinition)    aRunContext operationID   ignore]
lappend lBaselineConfiguration [list aRunContext(user)                 aRunContext originatorID  ignore]
lappend lBaselineConfiguration [list aRunContext(product)              aRunContext productID     ignore]
lappend lBaselineConfiguration [list aRunContext(displayID)            aRunContext sessionID     ignore]

# For App compat until they are updated
lappend lBaselineConfiguration [list aRunContext(entity)               aRunContext equipmentId   ignore]

lappend lBaselineConfiguration [list UtilityBaseline::sLogName         aRunContext equipmentId   ignore]
lappend lBaselineConfiguration [list UtilityBaseline::sToolName        aRunContext equipmentId   ignore]

#The event channel name to bind to
lappend lBaselineConfiguration [list InteractionBaseline::sEventBindingTopic aRunContext equipmentId  ignore]

# Needed to use the useother below
lappend lBaselineConfiguration [list sLotNumber                        aRunContext lotID         ignore]
lappend lBaselineConfiguration [list UtilityBaseline::sLotNumber       aRunContext lotID         ignore]
lappend lBaselineConfiguration [list UtilityBaseline::lLotNumberList   aRunContext lotNumberList [list "useother sLotNumber"]]
lappend lBaselineConfiguration [list UtilityBaseline::sOperation       aRunContext operationID   ignore]
lappend lBaselineConfiguration [list UtilityBaseline::sRoute           aRunContext route         [list "usevalue _default_"]]
lappend lBaselineConfiguration [list UtilityBaseline::sProduct         aRunContext productID     [list "usevalue _default_"]]
lappend lBaselineConfiguration [list UtilityBaseline::sLayerTableKey   aRunContext technology    ignore]
lappend lBaselineConfiguration [list UtilityBaseline::sFacility        aRunContext facility      ignore]

lappend lBaselineConfiguration [list UtilityBaseline::sDisplayGroup             immediate   "APC"]
lappend lBaselineConfiguration [list UtilityBaseline::sessionID                 immediate   ""]

lappend lBaselineConfiguration [list UtilityBaseline::nExtServicesTimeout       immediate   "60000"]
lappend lBaselineConfiguration [list InteractionBaseline::nCEIConnectionTimeout immediate   "150000"]

# worker process watchdog timeout (8hrs)
lappend lBaselineConfiguration [list UtilityBaseline::iWorkerProcessTimeout      immediate   "28800000"]

lappend lBaselineConfiguration [list UtilityBaseline::sPlan                     immediate   $sCatalystPlanName]

lappend lBaselineConfiguration [list InteractionBaseline::bUseAppConfig         immediate   "0"]

lappend lBaselineConfiguration [list lDefaultEmailList                          immediate   [list "daniel.steger@globalfoundries.com" ]]
lappend lBaselineConfiguration [list lErrorEmails                               immediate   [list "daniel.steger@globalfoundries.com" ]]
lappend lBaselineConfiguration [list lBaseErrorEmails                           immediate   [list "daniel.steger@globalfoundries.com" ]]

lappend lBaselineConfiguration [list apcDir                                     immediate   [file join $sCatalystRoot]]
lappend lBaselineConfiguration [list sLogDir                                    immediate   [file join $sCatalystRoot log]]
lappend lBaselineConfiguration [list sDataDir                                   immediate   [file join $sCatalystRoot log appdata]]
lappend lBaselineConfiguration [list UtilityBaseline::sRunLockDir               immediate   [file join $sCatalystRoot log running]]
lappend lBaselineConfiguration [list UtilityBaseline::lProductionNTs            immediate   ""]
lappend lBaselineConfiguration [list UtilityBaseline::sStagingNT                immediate   ""]
lappend lBaselineConfiguration [list UtilityBaseline::sUtilDir                  immediate   "e:/apcapps"]
lappend lBaselineConfiguration [list UtilityBaseline::sCjaProxyClass            immediate   "Catalyst::AMDCatalystControlJobAdmin"]
lappend lBaselineConfiguration [list InteractionBaseline::lPluginExtensions     immediate   ".alg.matlab7"]

lappend lBaselineConfiguration [list InteractionBaseline::bCdbCollisionNotify   immediate   0 ]
lappend lBaselineConfiguration [list InteractionBaseline::bUseCatalystParameterManagement immediate 0]
lappend lBaselineConfiguration [list ControlJobId                               immediate   "[info hostname]-[clock format [clock seconds] -format %Y%m%d.%H%M%S]-[pid]"]
lappend lBaselineConfiguration [list UtilityBaseline::bViewDeprecatedWarnings   immediate   "0"]
# remove obsolete sub lot types: CR42795 DEV-PC, PQUL, PROD
# remove CR45444   -- SiView subLotType: delete obsolete sublottypes in SiView (DEV-EM,PQUL-MW, PQUL-QD, PROD-PR, PROD-PX)
# remove CR45521   -- SiView subLotType: delete obsolete sublottypes in SiView (MW, PQUL-QE, PROD-PO, TQUL)
# remove CR47103   -- SiView subLotType: delete obsolete sublottypes in SiView (DEV, DEV-TS; PZ)
lappend lBaselineConfiguration [list UtilityBaseline::lSubLotTypes        immediate   [list DEV-EC DEV-ES DEV-RD PO PX PC PB PR QF QD QE QX EQ RQ ES EO EM EC ET EB RD SCRAP "Equipment Monitor" "Process Monitor" Correlation Dummy VRT Recycle VendorINTRA_28 VendorLager_29 VendorOPSPPL]]
lappend lBaselineConfiguration [list UtilityBaseline::lSubLotTypesProduction    immediate   [list "DEV-EC" "DEV-ES" "DEV-RD" "PO" "PX" "PC" "PB" "PR" "QF" "QD" "QE" "QX" "EQ" "RQ" "ES" "EO" "EM" "EC" "ET" "EB" "RD"]]

lappend lBaselineConfiguration [list FabSpecific::bUseSMTPMail                  immediate   "1"]
lappend lBaselineConfiguration [list APCBaseline::Email::sSMTPServer            immediate   "DRSSMTP"]
lappend lBaselineConfiguration [list APCBaseline::Email::nTimeout               immediate   "10"]
lappend lBaselineConfiguration [list APCBaseline::Email::sEmailSenderName       immediate   [regsub -nocase {^sf([[:digit:]]{2})([[:alpha:]]+)([[:digit:]]{2})$} [info hostname] {F\1_Dev\3}] ]
lappend lBaselineConfiguration [list APCBaseline::Email::nEmailCountDisableLimit immediate  "50"]
lappend lBaselineConfiguration [list APCBaseline::Email::nEmailCountWarningLimit immediate  "25"]


#
# Designation of APC port the script can receive events on
# Use port 80 for production / 8080 for development
lappend lBaselineConfiguration [list InteractionBaseline::sApcEventPort         immediate   "8080"]
lappend lBaselineConfiguration [list InteractionBaseline::bPollForEvents        immediate   "1"]

lappend lBaselineConfiguration [list UtilityBaseline::nFileSizeLimit            immediate   "2000000"]
lappend lBaselineConfiguration [list FabSpecific::sLogViewerURL                 immediate   "http://drsapc/apcsetup/AppLog/page_04.php"]
lappend lBaselineConfiguration [list FabSpecific::sCatalystPortalURL            immediate   "http://localhhost:8080/portal"]
lappend lBaselineConfiguration [list APCWebFTP::sAPCWebServer                   immediate   "localhost"]
lappend lBaselineConfiguration [list APCWebFTP::sUser                           immediate   "xxx"]
lappend lBaselineConfiguration [list APCWebFTP::sPassword                       immediate   "yyy"]

# The following defines a list of context items that will be used by all controllers in the fab for the "context" part
# of a ControlHistory event.  This list should be pairs of the source variable and what the column will be named in
# the history database.  The variables specified need to be visible in the global scope, or fully qualified.
#lappend lBaselineConfiguration [list ControlHistory::lContextItems immediate [list UtilityBaseline::sToolName entity UtilityBaseline::sLotNumber lotNumber UtilityBaseline::lLotNumberList lotNumberList UtilityBaseline::sOperation operation UtilityBaseline::sRoute route UtilityBaseline::sProduct product aLayerInfo(layer) layer]]
lappend lBaselineConfiguration [list ControlHistory::lContextItems immediate [list    UtilityBaseline::sToolName        "equipmentId" \
                                                                                      UtilityBaseline::sLotNumber       "lotID" \
                                                                                      UtilityBaseline::lLotNumberList   "lotIDList" \
                                                                                      aRunContext(familyLotID)          "familyLotID" \
                                                                                      aRunContext(carrierID)            "carrierID" \
                                                                                      FabSpecific::sLogControlJobId     "controlJobId" \
                                                                                      UtilityBaseline::sOperation       "operationID" \
                                                                                      aRunContext(operationNumber)      "operationNumber" \
                                                                                      aRunContext(originatorID)         "originatorID" \
                                                                                      UtilityBaseline::sRoute           "route" \
                                                                                      UtilityBaseline::sProduct         "productID" \
                                                                                      aRunContext(productGroupID)       "productGroupID" \
                                                                                      aRunContext(productType)          "productType" \
                                                                                      aRunContext(technology)           "technology" \
                                                                                      aLayerInfo(layer)                 "layer" \
                                                                                      ]]

# The following defines a list of context items that will be used by all controllers in the fab for the "context" part
# of an ApplicationHistory event.  This list should be a valid ApplicationHistory Params list.
# The variables specified need to be visible in the global scope, or fully qualified.
lappend lBaselineConfiguration [list ApplicationHistory::bEnabled       immediate "1"]
lappend lBaselineConfiguration [list ApplicationHistory::lContextParams immediate [list [list UtilityBaseline::sToolName        1 "equipmentId" "string"] \
                                                                                        [list UtilityBaseline::sLotNumber       1 "lotID"       "string"] \
                                                                                        [list UtilityBaseline::lLotNumberList   1 "lotIDList"   "string"] \
                                                                                        [list aRunContext(familyLotID)          1 "familyLotID" "string"] \
                                                                                        [list aRunContext(carrierID)            1 "carrierID"   "string"] \
                                                                                        [list FabSpecific::sLogControlJobId     1 "controlJobId" "string"] \
                                                                                        [list aRunContext(controlJobId)         1 "SiViewControlJobId" "string"] \
                                                                                        [list UtilityBaseline::sOperation       1 "operationID" "string"]\
                                                                                        [list aRunContext(operationNumber)      1 "operationNumber" "string"] \
                                                                                        [list aRunContext(originatorID)         1 "originatorID" "string"] \
                                                                                        [list UtilityBaseline::sRoute           1 "route"       "string"] \
                                                                                        [list UtilityBaseline::sProduct         1 "productID"   "string"] \
                                                                                        [list aRunContext(productGroupID)       1 "productGroupID" "string"] \
                                                                                        [list aRunContext(productType)          1 "productType" "string"] \
                                                                                        [list aRunContext(technology)           1 "technology"  "string"] \
                                                                                        [list aLayerInfo(layer)                 1 "layer"       "string"] \
                                                                                      ]]

#This will determine if the extended logging of each and every External Services Call shall be performed
lappend lBaselineConfiguration [list ExternalServices::bEnableExtendedLogging immediate 0]
lappend lBaselineConfiguration [list APCBaseline::CEI::bForwardRequestToJob         immediate 1]
lappend lBaselineConfiguration [list APCBaseline::CEI::lForwardRequestToJobContext  immediate [list "apcActionType" "CEISOAPForward"]]
lappend lBaselineConfiguration [list APCBaseline::CEI::sCEMHTTPLocator              immediate "HTTP:localhost:8080"]

#Similarly, these are values that are always stored in the data section of an event
#lappend lBaselineConfiguration [list ControlHistory::lDataItems immediate [list FabSpecific::sLogControlJobId CONTROLJOBID]]

#This will determine if data with value = NA is removed from a CH event, or stored as NA
lappend lBaselineConfiguration [list ControlHistory::bRemoveNA immediate 0]

#The CH event itself has a field for user, generally obtained from the run context
lappend lBaselineConfiguration [list ControlHistory::sUserEventField  aRunContext userID [list "usevalue _default_"]]

#
# CDB / RDB configuration settings
#
#For Fab36, only are going to use the RDB, so configure those variables here:
lappend lBaselineConfiguration [list DatabaseInterface::bReadFromCDB              immediate "0"]
lappend lBaselineConfiguration [list DatabaseInterface::bReadFromRDB              immediate "1"]
lappend lBaselineConfiguration [list DatabaseInterface::bWriteToCDB               immediate "0"]
lappend lBaselineConfiguration [list DatabaseInterface::bWriteToRDB               immediate "1"]
lappend lBaselineConfiguration [list DatabaseInterface::bDeleteFromCDBIfRDBExists immediate "0"]
lappend lBaselineConfiguration [list DatabaseInterface::sDatabasePrecedence immediate [list RDB]]
# disable lot data storage if layer is set to NA
lappend lBaselineConfiguration [list RDBImplementation::bDoNotStoreLotDataForLayerNA immediate "1"]
# enable RDB object caching
lappend lBaselineConfiguration [list RDBCache::bCacheEnabled                      immediate "1"]
# define RDB cache object expiration in minutes
lappend lBaselineConfiguration [list RDBCache::nCacheExpiration                   immediate "30"]
# custom record definition
lappend lBaselineConfiguration [list RDBImplementation::RDBCustomRecords::lGenericRDBTableSetup immediate [list \
                                                        "RTD" [list \
                                                            [list "LotID"             "lot_id"         "string" ] \
                                                            [list "ProcessDefinition" "pd"           "string" ] \
                                                            [list "EquipmentID"       "equipment_id" "string" ] \
                                                            ] \
                                                        "LMREQTAB" [list \
                                                            [list "ProcessDefinition" "pd"  "string" ] \
                                                            [list "EquipmentID"       "eqp" "string" ] \
                                                            ] \
                                                        "PCLMHIST" [list \
                                                            [list "LotID"   "lotid"     "string" ] \
                                                            [list "Type"    "type"      "string" ] \
                                                            [list "PL"      "pl"        "string" ] \
                                                            [list "PT"      "pt"        "string" ] \
                                                            ] \
                                                        "PCLQUEUE" [list \
                                                            [list "SpecID"      "specID"        "string" ] \
                                                            [list "Application" "application"   "string" ] \
                                                            [list "TableName"   "tablename"     "string" ] \
                                                            [list "ContextName" "contextname"   "string" ] \
                                                            [list "Type"        "type"          "string" ] \
                                                            [list "Thread"      "thread"        "string" ] \
                                                            [list "State"       "state"         "string" ] \
                                                            ] \
                                                            ]]


#To prevent errors with JobMode, set this here, so it ensures that the call gets made early on.  What this will do
#is call GetJobMode, which will query the plan properties and store the value for the sJobMode, and it will use
#that value for future calls
#lappend lBaselineConfiguration [list ::FabSpecific::sJobMode dummy dummy ignore "UtilityBaseline::GetJobMode"]
#all job request are treated as active, no listening jobs anymore due to SPC calculation and PCL
lappend lBaselineConfiguration [list ::FabSpecific::sJobMode immediate "Control"]

###################################################################
# Subsystem specific configurations
#
# FabGUI
# timeout set to 5 minutes
lappend lBaselineConfiguration [list APCBaseline::FabGUI::nDefaultTimeout       immediate   "300000"]
#
# Starview
#
lappend lBaselineConfiguration [list Starview::nTimeout                        immediate   "60000"]

# default configuration is used if no more specific rule match
lappend lBaselineConfiguration [list Starview::aStarviewHosts(Default)         immediate   [list "localhost" "3.1"]]

lappend lBaselineConfiguration [list Starview::aStarviewHosts(Fab36,SV3.1)     immediate   [list "localhost" "3.1"]]
lappend lBaselineConfiguration [list Starview::aDataChannelOnHost(Fab36,SV3.1) immediate   [list ""]]

############################################################################################
#
# Job Suspend / Resume
#
lappend lBaselineConfiguration [list APCBaseline::JobSuspend::lArraysToPersistOnSuspend  immediate  \
        [list aLotInfo aLayerInfo aLotData aControlParams aBatchData aControlResults aRequiredDataSetup aRequiredObsDataSetup \
              aGlobalExternalData aExternalData aThreadData aControlThreads aLastInputs aEventResults aObserverResults aRequiredSupervisoryDataSetup \
              aParameterManagementSetup ::ApplicationRegistryData::aApplications]]
lappend lBaselineConfiguration [list APCBaseline::JobSuspend::lVarsToPersistOnSuspend    immediate [list nEventLockTimeout sReleaseEventName bAbortOnEventTimeout bExpectEvents bSetupForMetrologyEvents bSendApcDone UtilityBaseline::bBatchRun]]
lappend lBaselineConfiguration [list APCBaseline::JobSuspend::lJobPersistenceKeyElements immediate [list aRunContext(equipmentId) aRunContext(controlJobId) aRunContext(lotID) aRunContext(operationID)]]
lappend lBaselineConfiguration [list APCBaseline::JobSuspend::lSerializationNotPermitted immediate [list aRunContext]]
# use misc table or job session table for job suspend session data
lappend lBaselineConfiguration [list APCBaseline::JobSuspend::bStoreSessionDataInMiscTable immediate 0]

############################################################################################
#
# FDC
#
lappend lBaselineConfiguration [list FDC::sDefaultAHTableNameForResults                 immediate "fdcres4"]

#lappend lBaselineConfiguration [list FDC::sTrendViewerURL                               immediate "http://sfc1edad02:3050/EDATrendViewer/EDATrendViewer.application"]
# use productive version of trendviewer even in ITDC
lappend lBaselineConfiguration [list FDC::sTrendViewerURL                               immediate "http://vf1edawebp01:3050/EDATrendViewer/EDATrendViewer.application"]

lappend lBaselineConfiguration [list FDC::sTrendViewerConnectTo                         immediate "ITDC"]
#lappend lBaselineConfiguration [list FDC::sTrendViewerConnectTo                         immediate "Fab1M1_Production"]

# RapidMiner integration
lappend lBaselineConfiguration [list APCBaseline::RapidMiner::bUseToolboxSoap      immediate 0 ]
lappend lBaselineConfiguration [list APCBaseline::RapidMiner::sCEMHTTPLocator      immediate "HTTP:localhost:8080" ]
lappend lBaselineConfiguration [list APCBaseline::RapidMiner::sRapidMinerUrl       immediate "http://localhost:8080/toolbox/soap" ]
lappend lBaselineConfiguration [list APCBaseline::RapidMiner::sUsername            immediate "X-CAT" ]
lappend lBaselineConfiguration [list APCBaseline::RapidMiner::sPassword            immediate "X-CAT" ]


############################################################################################
#
# PCL
#
lappend lBaselineConfiguration [list PCLDataCollection::lBatchDatacollectionEnabledTools      immediate [list FVX400 FVX401 FVX402 FVX403 FVX404 FVX1400 FVX1420] ]

############################################################################################
# ParseControllerInputData AbortType Definition
# @desc List of AbortTypes by source used in ParseControllerInputDataAbort (N/V Pairs of SourceArray/AbortType)
#
lappend lBaselineConfiguration [list UtilityBaseline::lParseControllerInputDataAbortTypes immediate [list \
            "aRunContext"          "BADCONTEXT" \
            "aLayerInfo"           "BADCONTEXT" \
            "aRmsParams"           "BADRECIPE" \
            "aRecipeParams"        "BADRECIPE" \
            "aParameters"          "BADRECIPE" \
            "aRawMetrologyData"    "BADDATA" \
            "aToolRecipe"          "NODATA" \
            "aLotData"             "NODATA" \
            "aParameterManagement" "BADCONFIG" \
            "aEventsData"          "BADDATA" \
            "EventsData"           "BADDATA" \
            "aWaferData"           "BADDATA" \
]]



############################################################################################
#
# Application Exception logging configuration
#
lappend lBaselineConfiguration [list FabSpecific::lApplicationExceptionClasses            immediate [list \
                                    "Unknown"               "Unknown and not properly handled exception."   \
                                    "NoData"                "No data available to perform requested operation."  \
                                    "BadData"               "Data quality not sufficient to perform requested operation."  \
                                    "ClassificationFail"    "Classification failure."   \
                                    "UnknownIdentifier"     "Unknown identifier used."  \
                                    "DCFail"                "Data collection failure."  \
                                    "DCFail_NoValuesAtAll"  "Data collection failure. No values at all received."  \
                                    "EventMaxRetryFail"     "Maximum number of retries was reached." \
                                    "EventExecutionFail"    "Event excution failure."      \
                                    "SVMissingEvents"       "Missing required Starview events." \
                                    "RecipeParameterSetup"  "Wrong or inconsistent recipe parameter setup." \
                                    "UnkownRecipe"          "Recipe not known by application." \
                                    "APCErrorExit"          "APCErrorExit call from application scope" \
                                    "PerformanceLimit"      "Application performance limit reached" \
                                    "ModelHandlingError"    "An error occured during model handling." \
                                    "InvalidArgument"       "An invalid argument was provided." \
                                    "ActionBindError"       "Creation of an action instance failed." \
                                    "ActionInternalError"   "Unhandled exception during action execution" \
                                    "ScriptAbort"           "Aborts an apc run (due to an error that was caught)" \
                                    "RMFail"                "Call to RapidMiner toolbox failed." \
                                    ]]



############################################################################################
#
# Job Performance Monitoring
#
# for global params use <param>,*
# for plan specific params use <param>,<plan>
#
# general
lappend lBaselineConfiguration [list FabSpecific::aPerformanceLimits(MemoryUsage,*)               immediate [list var "::FabSpecific::MemoryUsage" min 0 max 500000]]
lappend lBaselineConfiguration [list FabSpecific::aPerformanceLimits(CPUTime,*)                   immediate [list var "::FabSpecific::CPUTime"     min 0 max 600]]
lappend lBaselineConfiguration [list FabSpecific::aPerformanceLimits(WaitTimeUntilFirstEvent,*)   immediate [list var "::_aPerformanceData(WaitTimeUntilFirstEvent)" max 3600]]

############################################################################################
#
# Script version check Catalyst 3.2 vs Catalyst 3.4
#
set lDoNoCheckVersionsForScripts [list  ]
lappend lBaselineConfiguration [list FabSpecific::bEnableScriptVersionCheck     immediate "0"]
lappend lBaselineConfiguration [list FabSpecific::lIgnoreScriptNamesForCatalystVersionComparision     immediate $lDoNoCheckVersionsForScripts]

# procedure profiling definition - use only in Fab36 Development
# requires Baseline.AddOn.Profiling
#lappend lBaselineConfiguration [list UtilityBaseline::aProcProfiling(InteractionBaseline::ExecutePlugin)      immediate   "1"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcProfiling(RDBImplementation::StoreMiscRecord)      immediate   "1"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcProfiling(RDBImplementation::RetrieveMiscRecord)   immediate   "1"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcProfiling(InteractionBaseline::GetArrayFromDS)     immediate   "1"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcProfiling(InteractionBaseline::StoreArrayInDS)     immediate   "1"]

## config of Derdack message for failed trace data calls
lappend lBaselineConfiguration [list FabSpecific::bSendDerdackMsgOnTraceDateQueryFault          immediate 1]
lappend lBaselineConfiguration [list FabSpecific::nMinCallsDerdackMsgOnTraceDateQueryFault         immediate 10]
lappend lBaselineConfiguration [list FabSpecific::bIgnoreTWDerdackMsgOnTraceDateQueryFault      immediate 1]
lappend lBaselineConfiguration [list FabSpecific::lDisableDerdackMessagesForEquipment           immediate [list ]]
lappend lBaselineConfiguration [list FabSpecific::Derdack::sDerdackServerUrl                    immediate   "http://localhost/eawebservice/EventConnectorServer.ashx"]
lappend lBaselineConfiguration [list FabSpecific::Derdack::sUtilDir                             immediate   "e:/apcapps"]

# procedure specific debug levels - use only in Fab36 Development
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(UtilityBaseline::CleanUpAndExit)      immediate   "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(DatabaseInterface::CleanFamilyData)    immediate   "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(DatabaseInterface::CleanLotData)       immediate   "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(ControlBaseline::QueryModeledStates)       immediate   "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::FabGUI::ViewGenericQuery)   immediate   "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::FabGUI::ViewExtendedQuery)  immediate   "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(InteractionBaseline::StoreArrayInDS_lot) immediate   "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(InteractionBaseline::StoreArrayInDS_state) immediate   "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(InteractionBaseline::StoreArrayInDS)     immediate   "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(DataCollection::CheckExternalData)     immediate     "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(Starview::getEventData)                immediate     "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(SVAdapterCollection::RetrieveData)     immediate     "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(AnalysisEngine::Run)     immediate     "Debug"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(InteractionBaseline::WaitForEvents)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::JobSuspend::GetJobPersistenceKey)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::JobSuspend::Suspend)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::JobSuspend::Resume)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::JobSuspend::Serialize)     immediate     "Trace"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::JobSuspend::DeSerialize)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::JobSuspend::StartWaitingForEvents)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::MainScript::SuspendJob)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(APCBaseline::MainScript::ResumeJob)     immediate     "Verbose"]
#lappend lBaselineConfiguration [list UtilityBaseline::aProcLogLevels(RDBImplementation::RetrieveMiscRecord)     immediate     "Verbose"]
