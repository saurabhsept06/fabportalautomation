set nScriptVersion [lindex {$Revision$} 1]
set aScriptVersions($sScriptName) $nScriptVersion

namespace eval AppSimulator::TCLSMTPEmail {

    variable lEmailList [list "elke.birr@globalfoundries.com"]
    variable sExtraText ""
    variable counter 0

    proc setRecipients { lRecipients } {
        variable lEmailList
        set lEmailList $lRecipients
    }


    proc testSendEmailJava {} {
        variable lEmailList
        variable sExtraText
        $::oAmdAppLog writeTrace "Sending simple email to $lEmailList"
        set sTS [UtilityBaseline::MakeTimeStamp]
        set sSubject "Catalyst TEST Das ist eine Test-Email mit Java. Bitte ignorieren."
        append sMessage "This is a test email. Please ignore.\nElke Birr"
        append sMessage $sExtraText
        array set aResult [APCBaseline::Email::SendEmail $lEmailList $sSubject $sMessage]
        $::oAmdAppLog writeTrace "SendEmail completed"
        $::oAmdAppLog writeTrace [UtilityBaseline::PrintArray aResult]
    }
    
    proc testSendEmailTcl {} {
        variable lEmailList
        variable sExtraText
        $::oAmdAppLog writeTrace "Sending simple email to $lEmailList"
        set sTS [UtilityBaseline::MakeTimeStamp]
        set sSubject "Catalyst TEST Das ist eine Test-Email mit Tcl. Bitte ignorieren."
        append sMessage "This is a test email. Please ignore.\nElke Birr"
        append sMessage $sExtraText
        array set aResult [APCBaseline::Email::_SendEmailTcl [info hostname] $lEmailList $sSubject $sMessage]
        $::oAmdAppLog writeTrace "SendEmail completed"
        $::oAmdAppLog writeTrace [UtilityBaseline::PrintArray aResult]
    }

}

