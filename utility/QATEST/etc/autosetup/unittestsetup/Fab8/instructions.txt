$Id: instructions.txt 3361 2009-01-20 12:48:43Z dsteger $

HOW TO INSTALL THE UNIT TEST SETUP
Daniel Steger, daniel.stegerq@globalfoundries.com 
-----------------------------------------------------------

OK * check Carrier Categories: FEOL, MOL, BEOL and import others from setup.xls
* Equipment Type: UTXXXX
OK * check E10 states
OK * create Udata code MCSzone.UTZ1
OK * create Stocker from setup.xls
OK * check ReasonCode WAFERSCRAP.HUMA LotHoldRelease.ALL
OK * create sublottype for Production:
	UNT	"Unittest Production"	U	0
REMOVED * create sublottype for Equipment Monitors:
	UnittestMonitor	Equipment Monitor	EM	0
OK * check DEPARTMENT FAT
OK * check user group SH-NONE
OK * check MFGLayer A
OK * check STAGE NA
OK * create PRODUCT GROUP TEST
OK * create CARRIERS UT00 - UT20, FEOL (setup.xls), 
OK * create UTR1, UTR2, UTR3 (setup.xls)
OK  --> make them AVAILABLE in MM
OK * create BANK UT-RAW,UT-SHIP, AC-DISPO, AN-DISPO, UT-DISPO, UT-USE from setup.xls
OK * check Photo layer 01, 02, 05, 06, 80, CA
OK * import reticle groups from file utreticlegroups.csv and release
OK * import reticles from file utreticles.csv and release
* import reticlesets from file utreticlesets.csv and release

Skip * import dc defs from file utdcdefs.csv and release
Skip * import dc specs from file utdcspecs.csv and release
OK * Create equipment types MDX20X, FVX40X, MES50K, POL40X, SNK32X
OK * import equipments from file utequipments.csv and release
OK * import recipes from file utmrecipes.csv and release
OK * import logical recipes from file utlrecipes.csv and release
OK * import processes from file utprocesses.csv and release
OK * Pre1 Script UT-SKIP-COND.01 (Skips the next operation depending on a lot script parameter)
	"""IF (#OOC->Lot == "QATEST")
		Skip
	   EXIT
	"""
OK * import modules from file utmodules.csv and release (REMEASURE.01 Route may not exist)
OK * import main routes from file utroutes.csv, utproducts.csv and release
	this can be a painful task, since SM can not import routes, 
	when branch routes are defined which are not yet in the setup, 
	because they are also just imported
	help: just select UTRT001.01, clear all rework and branch routes
	import other routes and reconstruct branch and rework routes 

OK * add all UT* tools to work area UT-ALL
  create a area-group UT and add yourself, X-UNITTEST and UT-ALL to it

LoadPurposeType Test Setup
OK * Monitor Lot
  create an equipment monitor lot on route: UT-MN-UTC001.01, 
  put in carrier UT20
OK * Waiting Monitor Lot
  create a process monitor lot on route: UT-PR-UTI002.01
  put in carrier UT18
  forward to last operation of route
  move to bank UT-DISPO
OK * create a dummy lot on route: UT-DN-UTF001.01, 
  put in carrier UT19
  forward to last operation of route
  make sure it is in bank UT-USE
OK * make sure UTF002 is in state 2WPR

ChamberAvailability Test
* Monitor Lots
  move to UTPD000.01 (
  move to UTPD006.01
* Process Monitor Lot
  move to UTPD000.01
* Dummy Lot
  move to UTPD000.01