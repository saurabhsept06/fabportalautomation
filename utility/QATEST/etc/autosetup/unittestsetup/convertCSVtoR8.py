#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Convert SiView Setup CSVs from R6 to R8
    
    Daniel Steger
    
$ID
"""

import sys, re

def esplit(string, separator):
    list = []
    tmpStr = ""
    escaped = False
    for i in string:
        if i == '"':
            escaped = not escaped
        if not escaped and i == separator:
            list.append(tmpStr)
            tmpStr = ""
        else:
            tmpStr += i
    return list
            

def readfile (infile, outfile):
    for line in infile:
        if line.startswith('1'):
            elems = esplit(line.rstrip(), ',')
            pos = len(elems)-1
            type = elems[1].split('\\', 1)
            
            # Equipment
            if type[0] == "EQ01":
                # Instance name
                elems.insert(3, "")
                
                # underTrackStoragePriorityFlag
                elems.insert(23, "0")
                
                # whiteDefinitionFlag
                elems.insert(36, "0")
                
                # permissionRead
                elems.insert(40, "Public")
                
                # permissionReleaseDelete
                elems.insert(41, "Public")
                
                # userGroupRead    
                elems.insert(43, "")
                
                # userGroupReleaseDelete
                elems.insert(44, "")
            
            # Logical recipe
            elif type[0] == "EQ10":
                elems.insert(5, "")
                elems.insert(6, "0")
                elems.insert(pos, "")
                
             # Machine recipe
            elif type[0] == "EQ11":
                elems.insert(6, "")
                elems.insert(7, "0")
                elems.insert(pos, "")
            
            # PD
            elif type[0] == "PC01":
                elems.insert(6, "")
                elems.insert(7, "0")
                elems.insert(pos, "")
            
            # Module
            elif type[0] == "PC02":
                elems.insert(pos, "")
                
            # Route
            elif type[0] == "PC03":
                elems.insert(pos, "")
                
            # Product
            elif type[0] == "PR03":
                elems.insert(4,"0")
                elems.insert(pos, "")
                        
            else:
                print >> sys.stderr, "File type not recognized for %s" % infile
            
            for i in xrange(len(elems)):                
                if i != 0:
                    outfile.write(',')
                outfile.write(elems[i])
            outfile.write('\n')
        else:
            outfile.write(line)




def main():
    if len(sys.argv) < 1:
        sys.stderr("usage: %s <csv-files to convert>" % sys.argv[0])
        sys.exit(1)

    for f in sys.argv[1:]:
        convertedfile = file("R8_"+f, 'w')
        readfile(file(f, 'r'), convertedfile)        
        convertedfile.close()


if __name__ == '__main__':
    main()