#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Convert SiView Setup CSVs from R8 to R9
    
    Daniel Steger
    
$ID
"""

import sys, re

def esplit(string, separator):
    list = []
    tmpStr = ""
    escaped = False
    for i in string:
        if i == '"':
            escaped = not escaped
        if not escaped and i == separator:
            list.append(tmpStr)
            tmpStr = ""
        else:
            tmpStr += i
    return list
            

def readfile (infile, outfile):
    for line in infile:
        if line.startswith('1'):
            elems = esplit(line.rstrip(), ',')
            pos = len(elems)-1
            type = elems[1].split('\\', 1)
            
            # Equipment
            if type[0] == "EQ01":
                # Instance name
                elems.insert(6, "0")
                
                # waferSamplingPolicyName
                elems.insert(30, "Even")
                
                # waferSamplingAttribute
                elems.insert(31, "0")
	 
            else:
                print >> sys.stderr, "File type not recognized for %s" % infile
            
            for i in xrange(len(elems)):                
                if i != 0:
                    outfile.write(',')
                outfile.write(elems[i])
            outfile.write('\n')
	            if line.startswith('1'):
            elems = esplit(line.rstrip(), ',')
            pos = len(elems)-1
            type = elems[1].split('\\', 1)
     
            
            # Module
            elif type[0] == "PC02":
		#RecycleSamplingOperationNumber
                elems.insert(6, "")
                
            # Route
            elif type[0] == "PC03":
		#RecycleSamplingOperationNumber
                elems.insert(10, "")
               
      
            else:
                print >> sys.stderr, "File type not recognized for %s" % infile
            
            for i in xrange(len(elems)):                
                if i != 0:
                    outfile.write(',')
                outfile.write(elems[i])
            outfile.write('\n')

        else:
            outfile.write(line)




def main():
    if len(sys.argv) < 1:
        sys.stderr("usage: %s <csv-files to convert>" % sys.argv[0])
        sys.exit(1)

    for f in sys.argv[1:]:
        convertedfile = file("R10_"+f, 'w')
        readfile(file(f, 'r'), convertedfile)        
        convertedfile.close()


if __name__ == '__main__':
    main()