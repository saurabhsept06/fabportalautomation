#################
# LET Setup data
#################
#
# $$Id$$

username X-UNITTEST
startbank ON-RAW
shipbank OT-SHIP
sourceproduct OTHER.X
reticleset LETRETICLESET
usergroup G-FAT
schedulergroup SH-PLAN
idpattern %04d
maxentrycount 1000

# load equipment data, no templates used
loadequipmentids eqp_LEA-G-clean.csv
loadequipmentids eqp_LEH-O-clean.csv
loadequipmentids eqp_LEP-Y-clean.csv
loadequipmentids eqp_LEZ-clean.csv

# create machine recipes using these tools
# command prefix, count, min equipment, max equipments, choose sequencially or by random
createmrecipes MR 500 1 5 sequence
writemrecipes 03_recipes.csv true

# logical recipes
createlrecipes LET-LR 5000 sequence
writelrecipes 04_lrecipes.csv true

# process definitions
createprocesses LET-PD 5000 sequence
writeprocesses 05_processes.csv true

# module definitions
maxentrycount 100
createmodules LET-MD 500 10 50 sequence
writemodules 06_modules.csv true

# main defintions
maxentrycount 10
createroutes LET-MN 50 10 1500 sequence
writeroutes 07_routes.csv true

# products
maxentrycount 1000
createproducts LET-PROD 1000 sequence
writeproducts 08_products.csv true
